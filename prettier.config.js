module.exports = {
  arrowParens: "avoid",
  bracketSpacing: false,
  htmlWhitespaceSensitivity: "css",
  insertPragma: false,
  jsxBracketSameLine: true,
  jsxSingleQuote: false,
  parser: "babel",
  proseWrap: "preserve",
  requirePragma: false,
  semi: true,
  singleQuote: false,
  tabWidth: 2,
  trailingComma: "none",
  useTabs: false,
  printWidth: 120
};
