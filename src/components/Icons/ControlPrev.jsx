import React from "react";

export default ({width = 20, fill="white", style = {}, ...props}) => (
  <svg 
    viewBox="0 0 200 200"
    preserveAspectRatio="xMinYMin"
    fill="none" 
    xmlns="http://www.w3.org/2000/svg" 
    style={{...style, width}}
    {...props}>
    <rect width="42" height="147" transform="matrix(-1 0 0 1 63 26)" fill={fill}/>
    <path d="M178 26V173L63 99.5L178 26Z" fill={fill}/>
  </svg>
)