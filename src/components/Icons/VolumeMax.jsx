import React from "react";

export default ({width = 20, fill="white", style = {}, ...props}) => (
  <svg 
    viewBox="0 0 200 200"
    preserveAspectRatio="xMinYMin"
    fill="none" 
    xmlns="http://www.w3.org/2000/svg" 
    style={{...style, width}}
    {...props}>
    <path 
      fillRule="evenodd" 
      clipRule="evenodd" 
      d="M116.667 44.0833V26.9167C150.083 34.5 175 64.3333 175 100C175 135.667 150.083 165.5 116.667 173.083V155.917C140.75 148.75 158.333 126.417 158.333 100C158.333 73.5833 140.75 51.25 116.667 44.0833ZM25 125V75H58.3333L100 33.3333V166.667L58.3333 125H25ZM83.3333 126.417V73.5833L65.25 91.6666H41.6667V108.333H65.25L83.3333 126.417ZM137.5 100C137.5 85.25 129 72.5833 116.667 66.4167V133.5C129 127.417 137.5 114.75 137.5 100Z" 
      fill={fill}/>
  </svg>
);