import React from "react";

export default ({width = 20, fill = "white", style = {}, ...props}) => (
  <svg 
    viewBox="0 0 200 200" 
    preserveAspectRatio="xMinYMin" 
    fill="none" 
    xmlns="http://www.w3.org/2000/svg" 
    style={{...style, width}}
    {...props}>
    <path 
      d="M35.468 143.5C35.468 119.43 54.6842 100 78.4893 100C83.5085 100 88.3842 101.015 92.8298 102.61V13H164.532V42H121.511V143.935C121.224 167.715 102.151 187 78.4893 187C54.6842 187 35.468 167.57 35.468 143.5Z" 
      fill={ fill }/>
  </svg>
);