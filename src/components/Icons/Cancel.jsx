import React from "react";

export default ({width = 20, fill="white", style = {}, ...props}) => (
  <svg 
    viewBox="0 0 200 200"
    preserveAspectRatio="xMinYMin"
    fill="none" 
    xmlns="http://www.w3.org/2000/svg" 
    style={{...style, width}}
    {...props}>
    <path d="M142.855 57.5736L58.0022 142.426" stroke={fill} strokeWidth="12" strokeLinejoin="round"/>
    <path d="M58.0022 57.5736L142.855 142.426" stroke={fill} strokeWidth="12" strokeLinejoin="round"/>
  </svg>
);