import MusicalNote from "./MusicalNote";
import ControlPlay from "./ControlPlay";
import ControlPause from "./ControlPause";
import ControlNext from "./ControlNext";
import ControlPrev from "./ControlPrev";
import VolumeMax from "./VolumeMax";
import VolumeMed from "./VolumeMed";
import VolumeMin from "./VolumeMin";
import LogoFull from "./LogoFull";
import Home from "./Home";
import Search from "./Search";
import YourLibrary from "./YourLibrary";
import Plus from "./Plus";
import Cancel from "./Cancel";

export default {
  MusicalNote,
  LogoFull,
  Home,
  Search,
  YourLibrary,
  Plus,
  Cancel,
  ControlPlay,
  ControlPause,
  ControlPrev,
  ControlNext,
  VolumeMax,
  VolumeMed,
  VolumeMin
};
