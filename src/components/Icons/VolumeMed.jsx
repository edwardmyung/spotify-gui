import React from "react";

export default ({width = 20, fill="white", style = {}, ...props}) => (
  <svg 
    viewBox="0 0 200 200"
    preserveAspectRatio="xMinYMin"
    fill="none" 
    xmlns="http://www.w3.org/2000/svg" 
    style={{...style, width}}
    {...props}>
    <path 
      fillRule="evenodd" 
      clipRule="evenodd" 
      d="M25 75V125H58.3333L100 166.667V33.3333L58.3333 75H25ZM83.3333 73.5833V126.417L65.25 108.333H41.6667V91.6667H65.25L83.3333 73.5833ZM116.667 66.4167C129 72.5833 137.5 85.25 137.5 100C137.5 114.75 129 127.417 116.667 133.5V66.4167Z" 
      fill={fill} />
  </svg>
);