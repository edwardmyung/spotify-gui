import React from "react";

export default ({width = 20, fill="white", style = {}, ...props}) => (
  <svg 
    viewBox="0 0 200 200"
    preserveAspectRatio="xMinYMin"
    fill="none" 
    xmlns="http://www.w3.org/2000/svg" 
    style={{...style, width}}
    {...props}>
 
    <path 
      d="M100.429 40V160" 
      stroke={fill} 
      strokeWidth="12" 
      strokeLinejoin="round"/>

    <path 
      d="M40.4286 100L160.429 100" 
      stroke={fill} 
      strokeWidth="12" 
      strokeLinejoin="round"/>
  </svg>
);