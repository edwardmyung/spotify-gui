import React from "react";

export default ({ width = 20, fill="white", style = {}, ...props }) => (
  <svg 
    viewBox="0 0 200 200" 
    preserveAspectRatio="xMinYMin" 
    fill="none" 
    xmlns="http://www.w3.org/2000/svg" 
    style={{...style, width}}
    {...props}>
    <path d="M47 40V160" stroke={fill} strokeWidth="5" strokeLinejoin="round"/>
    <path d="M88.4286 40V160" stroke={fill} strokeWidth="5" strokeLinejoin="round"/>
    <path d="M115.571 40L167 160.714" stroke={fill} strokeWidth="5" strokeLinejoin="round"/>
  </svg>
);