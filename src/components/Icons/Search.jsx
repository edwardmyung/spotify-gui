import React from "react";

export default ({width = 20, fill="white", style = {}, ...props}) => (
  <svg 
    viewBox="0 0 200 200"
    preserveAspectRatio="xMinYMin"
    fill="none" 
    xmlns="http://www.w3.org/2000/svg" 
    style={{...style, width}}
    {...props}>
    <circle cx="95" cy="89" r="58.5" stroke={fill} strokeWidth="5"/>
    <path d="M130 135L166 171" stroke={fill} strokeWidth="5"/>
  </svg>
);