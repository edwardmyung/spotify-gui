import React from "react";

export default ({width = 20, fill="white", style = {}, ...props}) => (
  <svg 
    viewBox="0 0 200 200"
    preserveAspectRatio="xMinYMin"
    fill="none" 
    xmlns="http://www.w3.org/2000/svg" 
    style={{...style, width}}
    {...props}>
    <rect x="136" y="26" width="42" height="147" fill={fill}/>
    <path d="M21 26V173L136 99.5L21 26Z" fill={fill}/>
  </svg>
);