import React from "react";

export default ({width = 20, fill="white", style = {}, ...props}) => (
  <svg 
    viewBox="0 0 200 200"
    preserveAspectRatio="xMinYMin"
    fill="none" 
    xmlns="http://www.w3.org/2000/svg" 
    style={{...style, width}}
    {...props}>
    <path d="M33 176V61.4832L98.9848 25L167 61.4832V176H119.288V120.262H79.697V176H33Z" stroke={fill} strokeWidth="5"/>
  </svg>
);