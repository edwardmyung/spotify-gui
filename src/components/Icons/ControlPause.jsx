import React from "react";

export default ({width = 20, fill="white", style = {}, ...props}) => (
  <svg 
    viewBox="0 0 200 200"
    preserveAspectRatio="xMinYMin"
    fill="none" 
    xmlns="http://www.w3.org/2000/svg" 
    style={{...style, width}}
    {...props}>
    <rect x="44" y="27" width="42" height="147" fill={fill}/>
    <rect x="114" y="27" width="42" height="147" fill={fill}/>
  </svg>
);