import React, {useState} from "react";
import * as localStyle from "./style";
import Icons from "../Icons";
import {bindActionCreators} from "redux";
import {actions} from "../../redux/actions";
import {connect} from "react-redux";

const styles = {
  createPlaylistItem : {
    display: "flex", 
    alignItems : "center",
    fontWeight : 600,
    marginBottom : 15,
    cursor : "pointer"
  }
}

const Component = ({ createNewPlaylist }) => {
  const [showInput, setShowInput] = useState(false);
  const [newPlaylistName, setNewPlaylistName] = useState("");
  const [newPlaylistIsPublic, setNewPlaylistIsPublic] = useState(false);

  const onCreate = () => {
    if(newPlaylistName.length === 0) {
      // todo: toastr system
      console.log("Field cannot be empty")
      return;
    }
    createNewPlaylist(newPlaylistName, newPlaylistIsPublic);
    setNewPlaylistName("");
  }

  const createPlaylistButton = (
    <localStyle.PlaylistItem 
      onClick={() => setShowInput(true)}
      style={styles.createPlaylistItem}>
      <localStyle.AddPlaylistSquare>
        <Icons.Plus fill="black"/>
      </localStyle.AddPlaylistSquare>
      Create Playlist
    </localStyle.PlaylistItem>
  );

  const createPlaylistInputs = (
    <localStyle.PlaylistItem>
      <input 
        value={newPlaylistName} 
        onChange={ e => setNewPlaylistName(e.target.value) } 
        placeholder="Playlist name"/>
      <br/>
      <label>
        <input 
          type="checkbox" 
          checked={newPlaylistIsPublic} 
          onChange={e => setNewPlaylistIsPublic(e.target.checked)} />
        make public
      </label>
      <br/>
      <button onClick={ onCreate }>
        Create
      </button>
      <button onClick={ () => setShowInput(false) }>
        Cancel
      </button>
    </localStyle.PlaylistItem>
  )

  return (  
    <div>
      { showInput ? createPlaylistInputs : createPlaylistButton }
    </div>
  )
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => bindActionCreators({
  createNewPlaylist : actions.playlist.createNewPlaylist
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Component);