import React from "react";
import {Link} from "react-router-dom";
import globalStyle from "../style";
import * as localStyle from "./style";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {actions} from "../../redux/actions";
import {debounce} from "lodash";

class Component extends React.Component {
  constructor() {
    super();
    this.wrapperRef = React.createRef();
    this.onScroll = this.onScroll.bind(this);
  }
  
  onScroll() {
    const { scrollTop, scrollHeight, offsetHeight } = this.wrapperRef.current;
    const {fetchMorePlaylists} = this.props;
    console.log("furibg");
    if(scrollTop + offsetHeight > scrollHeight - 100) {
      fetchMorePlaylists();
    }
  }

  render() {
    const {playlists, unfollowPlaylist} = this.props;

    return (
      <globalStyle.layout.BodySidenavPlaylists 
        ref={this.wrapperRef} 
        onScroll={ debounce(this.onScroll, 400) }>
        {playlists.map(playlist => playlist && (
          <Link 
            to={`/playlist/${playlist.id}`} 
            key={playlist.id}>
            <localStyle.PlaylistItem>
              {playlist.name}
              <button onClick={ e => { e.preventDefault(); console.log("DELETING PLAYLIST"); unfollowPlaylist(playlist.id); }}>
                x
              </button>
            </localStyle.PlaylistItem>
          </Link>
        ))}
      </globalStyle.layout.BodySidenavPlaylists>
    );
  }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => bindActionCreators({
  unfollowPlaylist : actions.playlist.unfollowPlaylist
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Component);