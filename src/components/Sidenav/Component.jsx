import React, {useState} from "react";
import {Link} from "react-router-dom";
import {withRouter} from "react-router";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {actions} from "../../redux/actions";
import {PAGE_SIZE_PLAYLISTS_MINE} from "../../constants/view";

import MyPlaylists from "./MyPlaylists.jsx";
import CreateNewPlaylist from "./CreateNewPlaylist.jsx";
import Icons from "../Icons";
import globalStyle from "../style";
import * as localStyle from "./style";

const Sidenav = withRouter(({playlistsMine, fetchPlaylistTracks, fetchPlaylistsMine}) => {
  const [playlistsMinePage, setPlaylistsMinePage] = useState(1);

  const iconStyle={marginRight : 10};

  const navItems = <React.Fragment>
    <Link to="/browse/discover">
      <localStyle.SidenavItem>
        <Icons.Home width={25} style={iconStyle}/> Home
      </localStyle.SidenavItem>
    </Link>
    <Link to="/search">
      <localStyle.SidenavItem>
        <Icons.Search width={25} style={iconStyle}/> Search
      </localStyle.SidenavItem>
    </Link>
    <Link to="/collection/tracks">
      <localStyle.SidenavItem>
        <Icons.YourLibrary width={25} style={iconStyle}/> Your Library
      </localStyle.SidenavItem>
    </Link>
  </React.Fragment>;

  const playlists = <React.Fragment>
    <localStyle.PlaylistsHeading>
      Playlists
    </localStyle.PlaylistsHeading>
    <CreateNewPlaylist/>
    <MyPlaylists 
      playlists={playlistsMine} 
      fetchPlaylistTracks={fetchPlaylistTracks}
      fetchMorePlaylists={ () => {
        setPlaylistsMinePage(playlistsMinePage + 1);
        fetchPlaylistsMine(PAGE_SIZE_PLAYLISTS_MINE, playlistsMinePage * PAGE_SIZE_PLAYLISTS_MINE);
      }}/>
  </React.Fragment>;
  
  return (
    <globalStyle.layout.BodySidenav>
      <localStyle.Logo>
        <Icons.LogoFull />
      </localStyle.Logo>
      {navItems}  
      {playlists}
    </globalStyle.layout.BodySidenav>
  );
});

const mapStateToProps = state => ({
  playlistsMine : state.app.viewPlaylist.playlistsMineIds.map( id => state.entities.playlist[id]),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchPlaylistTracks: actions.playlist.fetchPlaylistTracks,
  fetchPlaylistsMine: actions.playlist.fetchPlaylistsMine
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Sidenav);