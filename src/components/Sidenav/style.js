import styled from "styled-components";

const PADDING = "8px 25px";

const ItemBase = styled.div`
  padding: ${PADDING};
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  transition: color 0.3s ease-in-out;
  color: ${p => p.theme.colors.greys.tone4};
  &:hover {
    color: ${p => p.theme.colors.white};
  }
`;

export const Logo = styled.div`
  padding: 30px 25px;
`;

export const SidenavItem = styled(ItemBase)`
  ${p => p.theme.typography.primary.p.bold};
  display: flex;
  align-items: center;
`;

export const PlaylistsHeading = styled.div`
  ${p => p.theme.typography.primary.pSmall.regularUppercase};
  padding: ${PADDING};
  color: ${p => p.theme.colors.greys.tone4};
  margin-top: 15px;
`;

export const PlaylistItem = styled(ItemBase)``;

export const AddPlaylistSquare = styled.div`
  background: rgba(255, 255, 255, 0.4);
  width: 30px;
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 3px;
  margin-right: 10px;
`;
