import React from "react";
import * as localStyle from "./style";
import Icons from "../Icons";

export default ({paused}) => {
  return (
    <localStyle.ControlsTrackWrapper>
      <Icons.ControlPrev onClick={ () => window.playerComponent.gotoPreviousTrack() } width={15}/>
      <localStyle.ControlsTrackCircle onClick={ () => window.playerComponent.togglePlay() }>
        {
          paused ? 
            <Icons.ControlPlay /> 
            : <Icons.ControlPause />
        }
      </localStyle.ControlsTrackCircle>
      <Icons.ControlNext onClick={ () => window.playerComponent.gotoNextTrack() } width={15}/>
    </localStyle.ControlsTrackWrapper>
  );
};
