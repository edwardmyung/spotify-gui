import styled from "styled-components";

const BAR_HEIGHT = 4;
const PERIPHERAL_WIDGET_WIDTH = 240; // wrappers for meta-info and volume

export const PlayerWrapper = styled.div`
  height: 90px;
  padding: 10px;
  background: ${p => p.theme.colors.darkGreys.tone1};
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const TrackMetaInfoWrapper = styled.div`
  display: flex;
  align-items: center;
  width: ${PERIPHERAL_WIDGET_WIDTH}px;
  overflow: hidden;
`;

export const TrackMetaInfoImage = styled.img`
  height: 70px;
  margin-right: 10px;
`;

export const TrackMetaInfoTrackName = styled.div`
  ${p => p.theme.typography.primary.p.bold}
  white-space:nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  width: 160px;
`;

export const TrackMetaInfoArtists = styled.div`
  ${p => p.theme.typography.primary.pSmall.regular}
`;

export const ProgressWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const ProgressTime = styled.div`
  ${p => p.theme.typography.primary.pSmall.regular}
  margin: 0px 10px;
`;

export const BarOuter = styled.div`
  width: ${p => p.widthPx}px;
  height: ${BAR_HEIGHT}px;
  border-radius: ${BAR_HEIGHT}px;
  background: rgba(255, 255, 255, 0.1);
  overflow: hidden;
`;

export const BarInner = styled.div`
  width: ${p => p.widthPct || 0}%;
  height: ${BAR_HEIGHT}px;
  background: rgba(255, 255, 255, 0.2);
  ${p => p.animate && "transition: width 1s linear;"}
`;

export const ControlsTrackWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 10px;
`;

export const ControlsTrackCircle = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 40px;
  margin: 0px 15px;
  border: solid 1px rgba(255, 255, 255, 0.4);
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ControlsVolumeWrapper = styled.div`
  display: flex;
  align-items: center;
`;
