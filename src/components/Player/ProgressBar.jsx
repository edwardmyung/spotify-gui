import React from "react";
import * as localStyle from "./style";
import helpers from "../../helpers";

export default class extends React.Component {
  constructor() {
    super();
    this.barOuterRef = React.createRef();
  }
  render() {
    const { position, duration } = this.props;

    const onClick = e => {
      const xPos = e.clientX;
      const barOuterLeftOffset = this.barOuterRef.current.getBoundingClientRect().left;

      let progress = (xPos - barOuterLeftOffset) / 400;
      window.playerComponent.seek(progress);
    }
    
    return (
      <localStyle.ProgressWrapper onClick={ onClick }>
        <localStyle.ProgressTime>{helpers.time.formatMsAsReadable(position)}</localStyle.ProgressTime>
        <localStyle.BarOuter widthPx={400} ref={this.barOuterRef}>
          <localStyle.BarInner widthPct={(position / duration) * 100 || 0 } animate/>
        </localStyle.BarOuter>
        <localStyle.ProgressTime>{helpers.time.formatMsAsReadable(duration)}</localStyle.ProgressTime>
      </localStyle.ProgressWrapper>
    );  
  }
} 