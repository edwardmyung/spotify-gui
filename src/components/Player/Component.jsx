import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {actions} from "../../redux/actions";
import TrackMetaInfo from "./TrackMetaInfo";
import ProgressBar from "./ProgressBar";
import ControlsTrack from "./ControlsTrack";
import ControlsVolume from "./ControlsVolume";
import * as localStyle from "./style";
import {interval} from "rxjs";

class Player extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      deviceId: "",
      paused: true,
      position: 0,
      duration: 0,
    };

    this.playerCheckInterval = null;

    this.gotoPreviousTrack = this.gotoPreviousTrack.bind(this);
    this.gotoNextTrack = this.gotoNextTrack.bind(this);
    this.togglePlay = this.togglePlay.bind(this);

    window.playerComponent = this;
  }

  componentDidMount() {
    if (this.props.token !== "") {
      this.playerCheckInterval = setInterval(() => this.checkForPlayer(), 1000);
    }
  }

  createEventHandlers() {
    this.player.on("initialization_error", e => console.error(e));
    this.player.on("authentication_error", e => console.error(e));
    this.player.on("account_error", e => console.error(e));
    this.player.on("playback_error", e => console.error(e));

    this.player.on("player_state_changed", state => {
      const trackId = state.track_window.current_track.linked_from.id ? 
        state.track_window.current_track.linked_from.id 
        : state.track_window.current_track.id;
      this.props.viewPlayerUpdateCurrentTrackId(trackId);
      
      this.setState({
        paused: state.paused,
        position : state.position,
        duration : state.duration
      });

      /*
        As this event listener can fire multiple times in quick succession. we must always
        unsubscribe the observable, before re-determining whether the observable should be 
      */
      if(this.positionObservable$) {
        this.positionObservable$.unsubscribe();
      }
      
      

      if(this.state.paused) {
        this.positionObservable$.unsubscribe();
      } else {
        this.positionObservable$ = interval(1000).subscribe( _x => {
          this.setState({position : this.state.position + 1000 });
        } );
      }
      
    });
  
    this.player.on('ready', ({ device_id }) => {
      console.log("Spotify player successfully connected, on device: ", device_id);
      this.setState({ deviceId: device_id });
      this.player.resume();
    });
  }


  checkForPlayer() {
    const { token } = this.props;
  
    if (window.Spotify !== null) {
      clearInterval(this.playerCheckInterval);

      this.player = new window.Spotify.Player({
        name: "Spotify Web API Project",
        getOAuthToken: cb => cb(token)
      });

      this.createEventHandlers();
      this.player.connect();
      this.player.getVolume().then(volume => this.setState({volume}));
    }
  }

  setTrack(albumOrPlaylistUri = null, trackUri = null, trackUris = []) {
    /*
      N.B. the body here can be either:
    
        1. JSON.stringify({uris: [...uris]})
        2. JSON.stringify({context_uri: "playlist|album-uri", offset : {*SEE DOCS*})
      
      This gives the player the context of which songs come after...
      
      https://developer.spotify.com/documentation/web-api/reference/player/start-a-users-playback/
    */ 
    let body;
    if(trackUris.length > 0) {
      body = JSON.stringify({
        uris: [...trackUris]
      })
    } else {
      body = JSON.stringify({ 
        context_uri: albumOrPlaylistUri,
        offset : { uri : trackUri}
      });
    }

    const {getOAuthToken} = this.player._options;
    return getOAuthToken(access_token => {
      fetch(`https://api.spotify.com/v1/me/player/play?device_id=${this.state.deviceId}`, {
        method: "PUT",
        body,
        headers: {
          "Content-Type" : "application/json",
          "Authorization": `Bearer ${access_token}`
        },
      }).then( () => {
        this.playTrack();
      });
    });
  }

  gotoPreviousTrack() { 
    this.player.previousTrack(); // temp: can chain .then here
    this.player.resume();
  }

  gotoNextTrack() { 
    this.player.nextTrack(); // temp: can chain .then here
    this.player.resume();
  }

  pauseTrack() { 
    this.player.pause();
  }

  playTrack() {
    this.player.resume();
  }

  togglePlay() {
    this.player.togglePlay();
  }

  setVolume(level) { // between 0 and 1
    this.player.setVolume(level);
    this.player.getVolume().then(volume => this.setState({ volume }));
  }

  seek(progress) { // between 0 and 1 
    this.player.seek(this.state.duration * progress).then(() => console.log("Updated"));
  }

  render() {
    const { 
      currentlyPlayingAlbumImageSrc, 
      currentlyPlayingTrack, 
      currentlyPlayingArtists
    } = this.props;

    if(currentlyPlayingTrack) {
      const formattedArtists = currentlyPlayingArtists.map(artist => artist.name).join(", ");
  
      return (
        <localStyle.PlayerWrapper>
          <TrackMetaInfo 
            albumImageSrc={currentlyPlayingAlbumImageSrc} 
            trackName={currentlyPlayingTrack.name} 
            artists={formattedArtists} />
          <div>
            <ControlsTrack paused={this.state.paused}/>
            <ProgressBar 
              position={this.state.position} 
              duration={this.state.duration} />
          </div>
          <ControlsVolume volume={this.state.volume}/>
        </localStyle.PlayerWrapper>
      );
    }

    return null;
  }
}

const mapStateToProps = state => {
  const currentlyPlayingTrack = state.app.viewPlayer.trackId && state.entities.track[state.app.viewPlayer.trackId]; 
  const currentlyPlayingAlbumImageSrc = state.app.viewPlayer.trackId 
    && currentlyPlayingTrack
    && state.entities.album
    && state.entities.album[currentlyPlayingTrack.album].images[0].url;
  
  const currentlyPlayingArtists = currentlyPlayingTrack && currentlyPlayingTrack.artists.map(artistId =>state.entities.artist[artistId] );
  
  return {
    token : state.app.token.token,
    currentlyPlayingAlbumImageSrc,
    currentlyPlayingTrack,
    currentlyPlayingArtists
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  viewPlayerUpdateCurrentTrackId : actions.viewPlayer.updateCurrentTrackId
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Player);
