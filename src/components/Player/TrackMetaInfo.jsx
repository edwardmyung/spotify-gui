import React from "react";
import * as localStyle from "./style";

export default ({albumImageSrc, trackName, artists}) => {
  return (
    <localStyle.TrackMetaInfoWrapper>
      <localStyle.TrackMetaInfoImage src={albumImageSrc} />
      <div>
        <localStyle.TrackMetaInfoTrackName>{trackName}</localStyle.TrackMetaInfoTrackName>
        <localStyle.TrackMetaInfoArtists>{artists}</localStyle.TrackMetaInfoArtists>
      </div>
    </localStyle.TrackMetaInfoWrapper>
  );
};
