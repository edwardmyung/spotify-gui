import React from "react";
import * as localStyle from "./style";
import Icons from "../Icons";

export default class extends React.Component {
  constructor() {
    super();
    this.barOuterRef = React.createRef();
  }

  renderVolumeIcon(volumeAsNumber) {
    if(volumeAsNumber === 0) {
      return <Icons.VolumeMin />;
    } 

    if(volumeAsNumber > 0.75) {
      return <Icons.VolumeMax />;
    }

    return <Icons.VolumeMed />;
  }

  render () {
    const { volume } = this.props
    
    // setVolume(0) returns volume as null, so need coerce to number if null
    let volumeAsNumber = volume || 0; 
    let volumeAsPct = (volumeAsNumber / 1) * 100; 

    const onClick = (e) => {
      let xPos = e.clientX;
      let barOuterLeftOffset = e.target.getBoundingClientRect().left;

      let newVolume = (xPos - barOuterLeftOffset) / 85;
      if (newVolume > 0.9) {
        newVolume = 1;
      } else if(newVolume < 0.1) {
        newVolume = 0;
      }

      window.playerComponent.setVolume(newVolume);
    }
  
    return (
      <localStyle.ControlsVolumeWrapper>
        <div 
          onClick={ () => window.playerComponent.setVolume(0) }
          style={{ marginRight : 10 }}>
          { this.renderVolumeIcon(volumeAsNumber) }
        </div>
        <localStyle.BarOuter 
          widthPx={85} 
          ref={this.barOuterRef} 
          onClick={ onClick }>
          <localStyle.BarInner widthPct={volumeAsPct}/>
        </localStyle.BarOuter>
      </localStyle.ControlsVolumeWrapper>
    );
  }
}
