import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  ${p => p.theme.typography.primary.p.regular};
  color: ${p => p.theme.colors.white};
`;

export const Body = styled.div`
  flex-grow: 1;
  display: flex;
  height: 0px;
`;

export const BodySidenav = styled.div`
  width: 240px;
  background: ${p => p.theme.colors.black};
  color: ${p => p.theme.colors.white};
  display: flex;
  flex-direction: column;
`;

export const BodySidenavPlaylists = styled.div`
  overflow: auto;
  ${p => p.theme.typography.primary.p.regular};
`;

export const BodyContent = styled.div`
  background: #151515;
  flex-grow: 1;
  overflow: auto;
`;

export const Container = styled.div`
  max-width: 1200px;
  padding: 15px;
  position: relative;
  margin: 0 auto;
`;

export const Footer = styled.div``;
