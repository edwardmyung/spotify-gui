import React from "react";
import styled from "styled-components";

const ButtonBase = styled.div`
  ${ p => p.theme.typography.primary.pSmall.regularUppercase }
  border-radius : 1000px;
  padding: 10px 40px;
  display:inline-block;
  user-select:none;
  cursor : ${ p => p.disabled ? "default" : "pointer"};
  
  transition: hover 0.06s cubic-bezier(.3,0,.7,1), background 0.3s ease-in-out;
  transform: scale(1);
  &:hover{
    transform: ${ p => p.disabled ? null : "scale(1.03)" };
  }
`;

const ButtonBrand = styled(ButtonBase)`
  background : ${ p => p.disabled ? "rgba(255,255,255,0.1)" : p.theme.colors.brand.main };
  &:hover {
    background : ${p => p.disabled ? "rgba(255,255,255,0.1)" : p.theme.colors.brand.hover};
  }
`

const ButtonClear = styled(ButtonBase)`
  background: none;
  border:solid 3px rgba(255,255,255,0.3);
`;

export default ({children, ...props}) => {
  if(props.disabled) {
    delete props.onClick;
  }

  if(props.type === "brand") {
    return <ButtonBrand {...props}>{children}</ButtonBrand>;
  } 

  return <ButtonClear {...props}>{children}</ButtonClear>;
}