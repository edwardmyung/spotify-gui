import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 10px 15px;
  cursor: default;
  user-select: none;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const TrackName = styled.div`
  ${p => p.theme.typography.primary.h5.bold}
`;

export const RowLeft = styled.div`
  padding-left: 10px;
  display: flex;
`;

export const RowLeftIconWrapper = styled.div``;

export const RowRight = styled.div``;

export const MiddotWrapper = styled.div`
  margin: 0px 6px;
`;

export const TextLinkHoverable = styled.div`
  color: ${p => p.theme.colors.greys.tone4};
  text-decoration: none;
  transition: all 0.3s ease-in-out;
  cursor: pointer;

  &:hover {
    color: ${p => p.theme.colors.white};
    text-decoration: underline;
  }
`;
