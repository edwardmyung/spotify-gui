import React from "react";
import * as localStyle from "./style";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import helpers from "../../helpers";
import Icons from "../Icons";

const Track = ({track, playlistId = null, trackUris = [], isCurrentlyPlaying, artists, album}) => {
  
  let artistLinks = artists.map( (artist, ix) => {
    const renderComma = artists.length > 1 && ix !== artists.length - 1;
    return (
      <Link to={`/artist/${artist.id}`} style={{minWidth : 0 }} key={ix}>
        <localStyle.TextLinkHoverable>
          {artist.name}
          {renderComma && <span>,&nbsp;</span>}
        </localStyle.TextLinkHoverable>
      </Link>
    ); 
  });

  const icon = isCurrentlyPlaying ? 
    <Icons.ControlPlay style={{ marginRight : 10 }}/>
    : <Icons.MusicalNote 
      fill="rgba(255,255,255,0.3)" 
      style={{ marginRight : 15, marginTop : 5 }} 
      width={15} 
      height={15} />
    

  const onDoubleClick = playlistId === null ? 
    () => {
      let startIx = trackUris.indexOf(track.uri);
      let reorderedTrackUris = trackUris.slice(startIx).concat(trackUris.slice(0,startIx));
      window.playerComponent.setTrack(null, null, reorderedTrackUris);
    } :
    () => {
      window.playerComponent.setTrack(`spotify:playlist:${playlistId}`, track.uri, []);
    }

  return (
    <localStyle.Wrapper key={track.id} onDoubleClick={ onDoubleClick }>
      
      <localStyle.RowLeft>
        <localStyle.RowLeftIconWrapper>
          {icon}
        </localStyle.RowLeftIconWrapper>
        <div>
          <localStyle.TrackName>
            {track.name}
          </localStyle.TrackName>

          <div style={{ display : "flex", overflow:"hidden" }}>
            {artistLinks}
            <localStyle.MiddotWrapper>
              &middot;
            </localStyle.MiddotWrapper>
            <Link to={`/album/${album && album.id}`}>
              <localStyle.TextLinkHoverable>
                {album && album.name}
              </localStyle.TextLinkHoverable>
            </Link>
          </div>
        </div>
      </localStyle.RowLeft>
      
      <localStyle.RowRight>
        {helpers.time.formatMsAsReadable(track.duration_ms)}
      </localStyle.RowRight>
    </localStyle.Wrapper>
  );
};

const mapStateToProps = (state, ownProps) => {
  const {track} = ownProps;
  const artists = track.artists.map( artistId => state.entities.artist[artistId]);
  const album = state.entities.album[ownProps.track.album];
  const isCurrentlyPlaying = state.app.viewPlayer.trackId === track.id;
  return {
    artists,
    album,
    isCurrentlyPlaying
  };
}

export default connect(mapStateToProps)(Track);