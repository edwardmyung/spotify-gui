import React from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom"; 
import colors from "../style-guidelines/colors";

const Wrapper = styled.div`
  margin: 10px;
  display: flex; 
  justify-content : center;
`;

const TabLink = styled(NavLink)`
  padding : 10px 25px;
  text-transform: uppercase;
  letter-spacing : 2px;
  ${ p => p.theme.typography.primary.pSmall.regularUppercase}
  
  border-bottom: solid 3px rgba(0,0,0,0);
  color : rgba(255,255,255,0.7);
  transition:color 0.3s ease-in-out;
  
  &:hover {
    color : rgba(255,255,255,1);
  }
`;

export default ({ tabData }) => (
  <Wrapper>
    {tabData.map(tab => (
      <TabLink 
        to={tab.linkTo} 
        key={tab.label} 
        activeStyle={{borderBottom: `solid 3px ${colors.brand.main}`}}>
        {tab.label}
      </TabLink>
    ))}
  </Wrapper> 
);
