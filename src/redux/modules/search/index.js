import * as searchActions from "./actions";
import searchReducer from "./reducer";

export const STATE_KEY = "search";
export const reducer = searchReducer;
export const actions = searchActions;
