import {normalize} from "normalizr";
import {addEntities} from "../entities/actions";

import {STATE_KEY as ARTIST_STATE_KEY} from "../artist";
import {STATE_KEY as TRACK_STATE_KEY} from "../track";
import {STATE_KEY as ALBUM_STATE_KEY} from "../album";
import {STATE_KEY as PLAYLIST_STATE_KEY} from "../playlist";
import {
  viewSearchAddAlbumIds,
  viewSearchAddArtistIds,
  viewSearchAddPlaylistIds,
  viewSearchAddTrackIds
} from "../view-search/actions";

export const FETCH_SEARCH_RESULTS_START = "FETCH_SEARCH_RESULTS_START";
export const FETCH_SEARCH_RESULTS_SUCCESS = "FETCH_SEARCH_RESULTS_SUCCESS";
export const FETCH_SEARCH_RESULTS_ERROR = "FETCH_SEARCH_RESULTS_ERROR";

export const fetchResults = (limit = 6, offset = 0, searchQuery = "") => (dispatch, getState, {requests, schema}) => {
  const {token} = getState().app.token;
  dispatch({type: FETCH_SEARCH_RESULTS_START});

  return fetch(requests.search.fetchResults(token, limit, offset, searchQuery))
    .then(res => res.json())
    .then(res => {
      if (res.albums.items.length > 0) {
        const data = normalize(res.albums.items, [schema.album]);
        dispatch(addEntities(ALBUM_STATE_KEY, data.entities.albums));
        dispatch(viewSearchAddAlbumIds(data.result));
      }

      if (res.artists.items.length > 0) {
        const data = normalize(res.artists.items, [schema.artist]);
        dispatch(addEntities(ARTIST_STATE_KEY, data.entities.artists));
        dispatch(viewSearchAddArtistIds(data.result));
      }

      if (res.tracks.items.length > 0) {
        const data = normalize(res.tracks.items, [schema.track]);
        dispatch(addEntities(TRACK_STATE_KEY, data.entities.tracks));
        dispatch(viewSearchAddTrackIds(data.result));
      }

      if (res.playlists.items.length > 0) {
        const data = normalize(res.playlists.items, [schema.playlist]);
        dispatch(addEntities(PLAYLIST_STATE_KEY, data.entities.playlists));
        dispatch(viewSearchAddPlaylistIds(data.result));
      }

      dispatch({type: FETCH_SEARCH_RESULTS_SUCCESS});
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_SEARCH_RESULTS_ERROR});
    });
};
