export const VIEW_ARTIST_ALBUM_IDS_ADD = "VIEW_ARTIST_ALBUM_IDS_ADD";
export const VIEW_ARTIST_TOP_TRACK_IDS_ADD = "VIEW_ARTIST_TOP_TRACK_IDS_ADD";
export const VIEW_ARTIST_RELATED_ARTIST_IDS_ADD = "VIEW_ARTIST_RELATED_ARTIST_IDS_ADD";

export const viewArtistAlbumIdsAdd = albumIds => ({
  type: VIEW_ARTIST_ALBUM_IDS_ADD,
  payload: {
    albumIds
  }
});

export const viewArtistTopTrackIdsAdd = trackIds => ({
  type: VIEW_ARTIST_TOP_TRACK_IDS_ADD,
  payload: {
    trackIds
  }
});

export const viewArtistRelatedArtistIdsAdd = artistIds => ({
  type: VIEW_ARTIST_RELATED_ARTIST_IDS_ADD,
  payload: {
    artistIds
  }
});
