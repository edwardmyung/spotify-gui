import * as viewArtistActions from "./actions";
import viewArtistReducer from "./reducer";

export const STATE_KEY = "viewArtist";
export const reducer = viewArtistReducer;
export const actions = viewArtistActions;
