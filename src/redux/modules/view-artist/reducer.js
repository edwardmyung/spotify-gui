import * as actions from "./actions";

const defaultState = {
  topTrackIds: [],
  albumIds: [],
  relatedArtistIds: []
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.VIEW_ARTIST_ALBUM_IDS_ADD:
      return {
        ...state,
        albumIds: action.payload.albumIds
      };

    case actions.VIEW_ARTIST_TOP_TRACK_IDS_ADD:
      return {
        ...state,
        topTrackIds: action.payload.trackIds
      };

    case actions.VIEW_ARTIST_RELATED_ARTIST_IDS_ADD:
      return {
        ...state,
        relatedArtistIds: action.payload.artistIds
      };

    default:
      return state;
  }
};
