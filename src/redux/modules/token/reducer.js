import {SET_TOKEN} from "./actions";

export default (state = {}, action) => {
  switch (action.type) {
    case SET_TOKEN:
      return {
        ...state,
        token: action.payload.token,
        expiry_dt: action.payload.expiry_dt
      };

    default:
      return state;
  }
};
