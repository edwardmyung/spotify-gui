export const SET_TOKEN = "SET_TOKEN";

export const setToken = payload => {
  return {
    type: SET_TOKEN,
    payload
  };
};
