import * as tokenActions from "./actions";
import tokenReducer from "./reducer";

export const actions = tokenActions;
export const reducer = tokenReducer;
export const STATE_KEY = "token";
