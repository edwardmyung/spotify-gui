export const VIEW_SEARCH_ADD_ALBUM_IDS = "VIEW_SEARCH_ADD_ALBUM_IDS";
export const VIEW_SEARCH_ADD_ARTIST_IDS = "VIEW_SEARCH_ADD_ARTIST_IDS";
export const VIEW_SEARCH_ADD_PLAYLIST_IDS = "VIEW_SEARCH_ADD_PLAYLIST_IDS";
export const VIEW_SEARCH_ADD_TRACK_IDS = "VIEW_SEARCH_ADD_TRACK_IDS";
export const VIEW_SEARCH_CLEAR_ALL_IDS = "VIEW_SEARCH_CLEAR_ALL_IDS";

export const viewSearchAddAlbumIds = albumIds => ({
  type: VIEW_SEARCH_ADD_ALBUM_IDS,
  payload: {albumIds}
});

export const viewSearchAddArtistIds = artistIds => ({
  type: VIEW_SEARCH_ADD_ARTIST_IDS,
  payload: {artistIds}
});

export const viewSearchAddPlaylistIds = playlistIds => ({
  type: VIEW_SEARCH_ADD_PLAYLIST_IDS,
  payload: {playlistIds}
});

export const viewSearchAddTrackIds = trackIds => ({
  type: VIEW_SEARCH_ADD_TRACK_IDS,
  payload: {trackIds}
});

export const viewSearchClearAllIds = () => ({
  type: VIEW_SEARCH_CLEAR_ALL_IDS
});
