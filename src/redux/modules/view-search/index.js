import * as viewSearchActions from "./actions";
import viewSearchReducer from "./reducer";

export const STATE_KEY = "viewSearch";
export const reducer = viewSearchReducer;
export const actions = viewSearchActions;
