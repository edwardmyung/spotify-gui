import * as actions from "./actions";

const defaultState = {
  artistIds: [],
  albumIds: [],
  playlistIds: [],
  trackIds: []
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.VIEW_SEARCH_ADD_PLAYLIST_IDS:
      return {
        ...state,
        playlistIds: action.payload.playlistIds
      };

    case actions.VIEW_SEARCH_ADD_TRACK_IDS:
      return {
        ...state,
        trackIds: action.payload.trackIds
      };

    case actions.VIEW_SEARCH_ADD_ALBUM_IDS:
      return {
        ...state,
        albumIds: action.payload.albumIds
      };

    case actions.VIEW_SEARCH_ADD_ARTIST_IDS:
      return {
        ...state,
        artistIds: action.payload.artistIds
      };

    case actions.VIEW_SEARCH_CLEAR_ALL_IDS:
      return defaultState;

    default:
      return state;
  }
};
