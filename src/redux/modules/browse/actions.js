import {normalize} from "normalizr";
import {addEntities} from "../entities/actions";
import {STATE_KEY as TRACK_STATE_KEY} from "../track";
import {STATE_KEY as ALBUM_STATE_KEY} from "../album";
import {STATE_KEY as ARTIST_STATE_KEY} from "../artist";
import {STATE_KEY as USER_STATE_KEY} from "../user";
import {STATE_KEY as PLAYLIST_STATE_KEY} from "../playlist";

import {
  viewBrowseClearDiscoverRecommendationIds,
  viewBrowseAddDiscoverRecommendationIds,
  viewBrowseAddGenrePlaylistIds,
  viewBrowseAddNewReleaseAlbumIds
} from "../view-browse/actions";

export const FETCH_BROWSE_GENRE_SEEDS_START = "FETCH_BROWSE_GENRE_SEEDS_START";
export const FETCH_BROWSE_GENRE_SEEDS_SUCCESS = "FETCH_BROWSE_GENRE_SEEDS_SUCCESS";
export const FETCH_BROWSE_GENRE_SEEDS_ERROR = "FETCH_BROWSE_GENRE_SEEDS_ERROR";
export const FETCH_BROWSE_RECOMMENDATIONS_START = "FETCH_BROWSE_RECOMMENDATIONS_START";
export const FETCH_BROWSE_RECOMMENDATIONS_SUCCESS = "FETCH_BROWSE_RECOMMENDATIONS_SUCCESS";
export const FETCH_BROWSE_RECOMMENDATIONS_ERROR = "FETCH_BROWSE_RECOMMENDATIONS_ERROR";
export const FETCH_BROWSE_CATEGORIES_START = "FETCH_BROWSE_CATEGORIES_START";
export const FETCH_BROWSE_CATEGORIES_SUCCESS = "FETCH_BROWSE_CATEGORIES_SUCCESS";
export const FETCH_BROWSE_CATEGORIES_ERROR = "FETCH_BROWSE_CATEGORIES_ERROR";
export const FETCH_BROWSE_CATEGORIES_PLAYLISTS_START = "FETCH_BROWSE_CATEGORIES_PLAYLISTS_START";
export const FETCH_BROWSE_CATEGORIES_PLAYLISTS_SUCCESS = "FETCH_BROWSE_CATEGORIES_PLAYLISTS_SUCCESS";
export const FETCH_BROWSE_CATEGORIES_PLAYLISTS_ERROR = "FETCH_BROWSE_CATEGORIES_PLAYLISTS_ERROR";
export const FETCH_BROWSE_NEW_RELEASE_ALBUMS_START = "FETCH_BROWSE_NEW_RELEASE_ALBUMS_START";
export const FETCH_BROWSE_NEW_RELEASE_ALBUMS_SUCCESS = "FETCH_BROWSE_NEW_RELEASE_ALBUMS_SUCCESS";
export const FETCH_BROWSE_NEW_RELEASE_ALBUMS_ERROR = "FETCH_BROWSE_NEW_RELEASE_ALBUMS_ERROR";

export const fetchBrowseGenreSeeds = () => (dispatch, getState, {requests}) => {
  const {token} = getState().app.token;
  dispatch({type: FETCH_BROWSE_GENRE_SEEDS_START});

  return fetch(requests.browse.fetchBrowseGenreSeeds(token))
    .then(res => res.json())
    .then(res => {
      dispatch({
        type: FETCH_BROWSE_GENRE_SEEDS_SUCCESS,
        payload: {
          genreSeeds: res.genres
        }
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_BROWSE_GENRE_SEEDS_ERROR});
    });
};

export const fetchBrowseRecommendations = (limit, marketCode, genreSeeds, targets, key) => (
  dispatch,
  getState,
  {requests, schema}
) => {
  const {token} = getState().app.token;
  dispatch({type: FETCH_BROWSE_RECOMMENDATIONS_START});

  return fetch(requests.browse.fetchBrowseRecommendations(token, limit, marketCode, genreSeeds, targets, key))
    .then(res => res.json())
    .then(res => {
      const data = normalize(res.tracks, [schema.track]);

      dispatch(addEntities(TRACK_STATE_KEY, data.entities.tracks));
      dispatch(addEntities(ARTIST_STATE_KEY, data.entities.artists));
      dispatch(addEntities(ALBUM_STATE_KEY, data.entities.albums));

      dispatch(viewBrowseClearDiscoverRecommendationIds());
      dispatch(viewBrowseAddDiscoverRecommendationIds(data.result));

      dispatch({type: FETCH_BROWSE_RECOMMENDATIONS_SUCCESS});
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_BROWSE_RECOMMENDATIONS_ERROR});
    });
};

export const fetchBrowseCategories = (limit = 50) => (dispatch, getState, {requests, schema}) => {
  const {token} = getState().app.token;

  dispatch({type: FETCH_BROWSE_CATEGORIES_START});

  return fetch(requests.browse.fetchBrowseCategories(token, limit))
    .then(res => res.json())
    .then(res => {
      dispatch({
        type: FETCH_BROWSE_CATEGORIES_SUCCESS,
        payload: {
          genreCategories: res.categories.items
        }
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_BROWSE_CATEGORIES_ERROR});
    });
};

export const fetchBrowseCategoriesPlaylists = (categoryId, limit = 50, offset = 0) => (
  dispatch,
  getState,
  {requests, schema}
) => {
  const {token} = getState().app.token;

  dispatch({type: FETCH_BROWSE_CATEGORIES_PLAYLISTS_START});

  return fetch(requests.browse.fetchBrowseCategoriesPlaylists(token, limit, offset, categoryId))
    .then(res => res.json())
    .then(res => {
      const data = normalize(res.playlists.items, [schema.playlist]);
      dispatch(addEntities(USER_STATE_KEY, data.entities.users));
      dispatch(addEntities(PLAYLIST_STATE_KEY, data.entities.playlists));
      dispatch(viewBrowseAddGenrePlaylistIds(data.result));
      dispatch({
        type: FETCH_BROWSE_CATEGORIES_PLAYLISTS_SUCCESS
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_BROWSE_CATEGORIES_PLAYLISTS_ERROR});
    });
};

export const fetchBrowseNewReleaseAlbums = (limit = 20, offset = 0) => (dispatch, getState, {requests, schema}) => {
  const {token} = getState().app.token;

  dispatch({type: FETCH_BROWSE_NEW_RELEASE_ALBUMS_START});

  return fetch(requests.browse.fetchBrowseNewReleaseAlbums(token, limit, offset))
    .then(res => res.json())
    .then(res => {
      const data = normalize(res.albums.items, [schema.album]);
      dispatch(addEntities(ARTIST_STATE_KEY, data.entities.artists));
      dispatch(addEntities(ALBUM_STATE_KEY, data.entities.albums));
      dispatch(viewBrowseAddNewReleaseAlbumIds(data.result));
      dispatch({type: FETCH_BROWSE_NEW_RELEASE_ALBUMS_SUCCESS});
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_BROWSE_NEW_RELEASE_ALBUMS_ERROR});
    });
};
