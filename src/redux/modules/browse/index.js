import browseReducer from "./reducer";
import * as browseActions from "./actions";

export const STATE_KEY = "browse";
export const reducer = browseReducer;
export const actions = browseActions;
