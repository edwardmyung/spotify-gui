import * as actions from "./actions";

const defaultState = {
  /* /browse/discover */
  discoverRecommendationsLoading: false,
  genreSeedsLoading: true,
  genreSeeds: [],

  /* /browse/genres */
  genreCategoriesLoading: true,
  genreCategories: [],

  /* /genre/{genre} */
  genrePlaylistsLoading: true
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.FETCH_BROWSE_GENRE_SEEDS_START:
      return {
        ...state,
        genreSeedsLoading: true
      };

    case actions.FETCH_BROWSE_GENRE_SEEDS_SUCCESS:
      return {
        ...state,
        genreSeedsLoading: false,
        genreSeeds: action.payload.genreSeeds
      };

    case actions.FETCH_BROWSE_GENRE_SEEDS_ERROR:
      return {
        ...state,
        genreSeedsLoading: false
      };

    case actions.FETCH_BROWSE_RECOMMENDATIONS_START:
      return {
        ...state,
        discoverRecommendationsLoading: true
      };

    case actions.FETCH_BROWSE_RECOMMENDATIONS_SUCCESS:
    case actions.FETCH_BROWSE_RECOMMENDATIONS_ERROR:
      return {
        ...state,
        discoverRecommendationsLoading: false
      };

    case actions.FETCH_BROWSE_CATEGORIES_START:
      return {
        ...state,
        genreCategoriesLoading: true
      };

    case actions.FETCH_BROWSE_CATEGORIES_SUCCESS:
      return {
        ...state,
        genreCategoriesLoading: false,
        genreCategories: action.payload.genreCategories
      };

    case actions.FETCH_BROWSE_CATEGORIES_ERROR:
      return {
        ...state,
        genreCategoriesLoading: false
      };

    case actions.FETCH_BROWSE_CATEGORIES_PLAYLISTS_START:
      return {
        ...state,
        genrePlaylistsLoading: true
      };

    case actions.FETCH_BROWSE_CATEGORIES_PLAYLISTS_SUCCESS:
    case actions.FETCH_BROWSE_CATEGORIES_PLAYLISTS_ERROR:
      return {
        ...state,
        genrePlaylistsLoading: false
      };

    default:
      return state;
  }
};
