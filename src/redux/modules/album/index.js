import * as albumActions from "./actions";
import albumReducer from "./reducer";

export const STATE_KEY = "album";
export const actions = albumActions;
export const reducer = albumReducer;
