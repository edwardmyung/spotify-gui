import {normalize} from "normalizr";
import {addEntities} from "../entities/actions";
import {STATE_KEY as TRACK_STATE_KEY} from "../track";
import {STATE_KEY as ARTIST_STATE_KEY} from "../artist";
import {viewAlbumClearTrackIds, viewAlbumAddTrackIds} from "../view-album/actions";

export const FETCH_ALBUM_TRACKS_START = "FETCH_ALBUM_TRACKS_START";
export const FETCH_ALBUM_TRACKS_SUCCESS = "FETCH_ALBUM_TRACKS_SUCCESS";
export const FETCH_ALBUM_TRACKS_ERROR = "FETCH_ALBUM_TRACKS_ERROR";

export const fetchAlbumTracks = (albumId, limit = 50, offset = 0) => (
  dispatch,
  getState,
  {requests, schema}
) => {
  const {token} = getState().app.token;
  dispatch({type: FETCH_ALBUM_TRACKS_START});

  return fetch(requests.album.fetchAlbumTracks(token, albumId, limit, offset))
    .then(res => res.json())
    .then(res => {
      const data = normalize(res.items, [schema.track]);
      dispatch(addEntities(TRACK_STATE_KEY, data.entities.tracks));
      dispatch(addEntities(ARTIST_STATE_KEY, data.entities.artists));
      dispatch(viewAlbumClearTrackIds());
      dispatch(viewAlbumAddTrackIds(data.result));
      dispatch({type: FETCH_ALBUM_TRACKS_SUCCESS});
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_ALBUM_TRACKS_ERROR});
    });
};
