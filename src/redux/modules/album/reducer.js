import * as actions from "./actions";

const defaultState = {
  albumTracksLoading: false
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.FETCH_ALBUM_TRACKS_START:
      return {
        ...state,
        albumTracksLoading: true
      };

    case actions.FETCH_ALBUM_TRACKS_SUCCESS:
    case actions.FETCH_ALBUM_TRACKS_ERROR:
      return {
        ...state,
        albumTracksLoading: false
      };

    default:
      return state;
  }
};
