import viewBrowseReducer from "./reducer";
import * as viewBrowseActions from "./actions";

export const STATE_KEY = "viewBrowse";
export const reducer = viewBrowseReducer;
export const actions = viewBrowseActions;
