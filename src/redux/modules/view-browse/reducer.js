import * as actions from "./actions";
import uniq from "lodash/uniq";

const defaultState = {
  discoverRecommendationTrackIds: [],
  genrePlaylistIds: [],
  newReleaseAlbumIds: []
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.VIEW_BROWSE_CLEAR_DISCOVER_RECOMMENDATION_IDS:
      return {
        ...state,
        discoverRecommendationTrackIds: []
      };

    case actions.VIEW_BROWSE_ADD_DISCOVER_RECOMMENDATION_IDS:
      return {
        ...state,
        discoverRecommendationTrackIds: action.payload.trackIds
      };

    case actions.VIEW_BROWSE_ADD_GENRE_PLAYLIST_IDS:
      return {
        ...state,
        genrePlaylistIds: uniq([...state.genrePlaylistIds, ...action.payload.playlistIds])
      };

    case actions.VIEW_BROWSE_ADD_NEW_RELEASE_ALBUM_IDS:
      return {
        ...state,
        newReleaseAlbumIds: action.payload.albumIds
      };

    default:
      return state;
  }
};
