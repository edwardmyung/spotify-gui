export const VIEW_BROWSE_ADD_DISCOVER_RECOMMENDATION_IDS = "VIEW_BROWSE_ADD_DISCOVER_RECOMMENDATION_IDS";
export const VIEW_BROWSE_CLEAR_DISCOVER_RECOMMENDATION_IDS = "VIEW_BROWSE_CLEAR_DISCOVER_RECOMMENDATION_IDS";
export const VIEW_BROWSE_ADD_GENRE_PLAYLIST_IDS = "VIEW_BROWSE_ADD_GENRE_PLAYLIST_IDS";
export const VIEW_BROWSE_ADD_NEW_RELEASE_ALBUM_IDS = "VIEW_BROWSE_ADD_NEW_RELEASE_ALBUM_IDS";

export const viewBrowseAddDiscoverRecommendationIds = trackIds => ({
  type: VIEW_BROWSE_ADD_DISCOVER_RECOMMENDATION_IDS,
  payload: {
    trackIds
  }
});

export const viewBrowseClearDiscoverRecommendationIds = () => ({
  type: VIEW_BROWSE_CLEAR_DISCOVER_RECOMMENDATION_IDS
});

export const viewBrowseAddGenrePlaylistIds = playlistIds => ({
  type: VIEW_BROWSE_ADD_GENRE_PLAYLIST_IDS,
  payload: {playlistIds}
});

export const viewBrowseAddNewReleaseAlbumIds = albumIds => ({
  type: VIEW_BROWSE_ADD_NEW_RELEASE_ALBUM_IDS,
  payload: {albumIds}
});
