import * as viewPlayerActions from "./actions";
import viewPlayerReducer from "./reducer";

export const STATE_KEY = "viewPlayer";
export const reducer = viewPlayerReducer;
export const actions = viewPlayerActions;
