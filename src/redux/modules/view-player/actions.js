export const UPDATE_VIEW_PLAYER_CURRENT_TRACK_ID = "UPDATE_VIEW_PLAYER_CURRENT_TRACK_ID";

export const updateCurrentTrackId = trackId => dispatch => {
  dispatch({
    type: UPDATE_VIEW_PLAYER_CURRENT_TRACK_ID,
    payload: trackId
  });
};
