import {UPDATE_VIEW_PLAYER_CURRENT_TRACK_ID} from "./actions";

const defaultState = {
  trackId: ""
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case UPDATE_VIEW_PLAYER_CURRENT_TRACK_ID:
      return {
        ...state,
        trackId: action.payload
      };

    default:
      return state;
  }
};
