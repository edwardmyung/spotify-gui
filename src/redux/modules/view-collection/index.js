import viewCollectionReducer from "./reducer";
import * as viewCollectionActions from "./actions";

export const STATE_KEY = "viewCollection";
export const reducer = viewCollectionReducer;
export const actions = viewCollectionActions;
