export const VIEW_COLLECTION_ADD_TOP_TRACK_IDS = "VIEW_COLLECTION_ADD_TOP_TRACK_IDS";
export const VIEW_COLLECTION_ADD_TOP_ARTIST_IDS = "VIEW_COLLECTION_ADD_TOP_ARTIST_IDS";

export const viewCollectionAddTopTrackIds = trackIds => ({
  type: VIEW_COLLECTION_ADD_TOP_TRACK_IDS,
  payload: {trackIds}
});

export const viewCollectionAddTopArtistIds = artistIds => ({
  type: VIEW_COLLECTION_ADD_TOP_ARTIST_IDS,
  payload: {artistIds}
});
