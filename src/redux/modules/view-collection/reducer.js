import * as actions from "./actions";
import {uniq} from "lodash";

const defaultState = {
  topTrackIds: [],
  topArtistIds: []
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.VIEW_COLLECTION_ADD_TOP_TRACK_IDS:
      return {
        ...state,
        topTrackIds: uniq([...state.topTrackIds, ...action.payload.trackIds])
      };

    case actions.VIEW_COLLECTION_ADD_TOP_ARTIST_IDS:
      return {
        ...state,
        topArtistIds: uniq([...state.topArtistIds, ...action.payload.artistIds])
      };

    default:
      return state;
  }
};
