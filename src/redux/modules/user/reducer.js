import * as actions from "./actions";

const defaultState = {
  currentUserId: "",
  currentUserTopTracksLoading: true,
  currentUserTopArtistsLoading: true
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.FETCH_CURRENT_USER_TOP_ARTISTS_START:
      return {
        ...state,
        currentUserTopArtistsLoading: true
      };

    case actions.FETCH_CURRENT_USER_TOP_ARTISTS_SUCCESS:
    case actions.FETCH_CURRENT_USER_TOP_ARTISTS_ERROR:
      return {
        ...state,
        currentUserTopArtistsLoading: false
      };

    case actions.FETCH_CURRENT_USER_TOP_TRACKS_START:
      return {
        ...state,
        currentUserTopTracksLoading: true
      };

    case actions.FETCH_CURRENT_USER_TOP_TRACKS_SUCCESS:
    case actions.FETCH_CURRENT_USER_TOP_TRACKS_ERROR:
      return {
        ...state,
        currentUserTopTracksLoading: false
      };

    case actions.FETCH_CURRENT_USER_SUCCESS:
      return {
        ...state,
        currentUserId: action.payload.currentUserId
      };
    default:
      return state;
  }
};
