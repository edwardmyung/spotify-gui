import {normalize} from "normalizr";
import {addEntities} from "../entities/actions";
import {STATE_KEY as USER_STATE_KEY} from "./index";
import {STATE_KEY as ARTIST_STATE_KEY} from "../artist";
import {STATE_KEY as ALBUM_STATE_KEY} from "../album";
import {STATE_KEY as TRACK_STATE_KEY} from "../track";
import {viewCollectionAddTopTrackIds, viewCollectionAddTopArtistIds} from "../view-collection/actions";

export const FETCH_CURRENT_USER_START = "FETCH_CURRENT_USER_START";
export const FETCH_CURRENT_USER_SUCCESS = "FETCH_CURRENT_USER_SUCCESS";
export const FETCH_CURRENT_USER_ERROR = "FETCH_CURRENT_USER_ERROR";
export const FETCH_CURRENT_USER_TOP_TRACKS_START = "FETCH_CURRENT_USER_TOP_TRACKS_START";
export const FETCH_CURRENT_USER_TOP_TRACKS_SUCCESS = "FETCH_CURRENT_USER_TOP_TRACKS_SUCCESS";
export const FETCH_CURRENT_USER_TOP_TRACKS_ERROR = "FETCH_CURRENT_USER_TOP_TRACKS_ERROR";
export const FETCH_CURRENT_USER_TOP_ARTISTS_START = "FETCH_CURRENT_USER_TOP_ARTISTS_START";
export const FETCH_CURRENT_USER_TOP_ARTISTS_SUCCESS = "FETCH_CURRENT_USER_TOP_ARTISTS_SUCCESS";
export const FETCH_CURRENT_USER_TOP_ARTISTS_ERROR = "FETCH_CURRENT_USER_TOP_ARTISTS_ERROR";

export const fetchCurrentUser = () => (dispatch, getState, {requests, schema}) => {
  const {token} = getState().app.token;
  dispatch({type: FETCH_CURRENT_USER_START});

  return fetch(requests.user.fetchCurrentUser(token))
    .then(res => res.json())
    .then(user => {
      const data = normalize(user, schema.user);
      dispatch(addEntities(USER_STATE_KEY, data.entities.users));
      dispatch({
        type: FETCH_CURRENT_USER_SUCCESS,
        payload: {
          currentUserId: data.result
        }
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_CURRENT_USER_ERROR});
    });
};

export const fetchCurrentUserTopArtists = (limit = 20, offset = 0) => (dispatch, getState, {requests, schema}) => {
  const {token} = getState().app.token;
  dispatch({type: FETCH_CURRENT_USER_TOP_ARTISTS_START});

  return fetch(requests.user.fetchCurrentUserTopArtists(token, limit, offset))
    .then(res => res.json())
    .then(res => {
      const data = normalize(res.items, [schema.artist]);
      dispatch(addEntities(ARTIST_STATE_KEY, data.entities.artists));
      dispatch(viewCollectionAddTopArtistIds(data.result));
      dispatch({type: FETCH_CURRENT_USER_TOP_ARTISTS_SUCCESS});
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_CURRENT_USER_TOP_ARTISTS_ERROR});
    });
};

export const fetchCurrentUserTopTracks = (limit = 20, offset = 0) => (dispatch, getState, {requests, schema}) => {
  const {token} = getState().app.token;
  dispatch({type: FETCH_CURRENT_USER_TOP_TRACKS_START});

  return fetch(requests.user.fetchCurrentUserTopTracks(token, limit, offset))
    .then(res => res.json())
    .then(res => {
      const data = normalize(res.items, [schema.track]);
      dispatch(addEntities(ARTIST_STATE_KEY, data.entities.artists));
      dispatch(addEntities(ALBUM_STATE_KEY, data.entities.albums));
      dispatch(addEntities(TRACK_STATE_KEY, data.entities.tracks));
      dispatch(viewCollectionAddTopTrackIds(data.result));
      dispatch({type: FETCH_CURRENT_USER_TOP_TRACKS_SUCCESS});
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_CURRENT_USER_TOP_TRACKS_ERROR});
    });
};
