import * as userActions from "./actions";
import userReducer from "./reducer";

export const STATE_KEY = "user";
export const reducer = userReducer;
export const actions = userActions;
