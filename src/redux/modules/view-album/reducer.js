import * as actions from "./actions";

const defaultState = {
  trackIds: []
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.VIEW_ALBUM_ADD_TRACK_IDS:
      return {
        ...state,
        trackIds: [...state.trackIds, ...action.payload.trackIds]
      };

    case actions.VIEW_ALBUM_CLEAR_TRACK_IDS:
      return {
        ...state,
        trackIds: []
      };

    default:
      return state;
  }
};
