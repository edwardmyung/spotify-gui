export const VIEW_ALBUM_ADD_TRACK_IDS = "VIEW_ALBUM_ADD_TRACK_IDS";
export const VIEW_ALBUM_CLEAR_TRACK_IDS = "VIEW_ALBUM_CLEAR_TRACK_IDS";

export const viewAlbumAddTrackIds = trackIds => ({
  type: VIEW_ALBUM_ADD_TRACK_IDS,
  payload: {
    trackIds
  }
});

export const viewAlbumClearTrackIds = () => ({
  type: VIEW_ALBUM_CLEAR_TRACK_IDS
});
