import * as viewAlbumActions from "./actions";
import viewAlbumReducer from "./reducer";

export const STATE_KEY = "viewAlbum";
export const reducer = viewAlbumReducer;
export const actions = viewAlbumActions;
