import * as actions from "./actions";

const defaultState = {
  artistFullLoading: true
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.FETCH_ARTIST_FULL_ERROR:
    case actions.FETCH_ARTIST_FULL_SUCCESS:
      return {
        ...state,
        artistFullLoading: false
      };

    case actions.FETCH_ARTIST_FULL_START:
      return {
        ...state,
        artistFullLoading: true
      };

    default:
      return state;
  }
};
