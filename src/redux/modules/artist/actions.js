import {normalize} from "normalizr";
import {addEntities} from "../entities/actions";
import {STATE_KEY as ARTIST_STATE_KEY} from "./index";
import {STATE_KEY as ALBUM_STATE_KEY} from "../album";
import {STATE_KEY as TRACK_STATE_KEY} from "../track";
import {
  viewArtistAlbumIdsAdd,
  viewArtistTopTrackIdsAdd,
  viewArtistRelatedArtistIdsAdd
} from "../view-artist/actions";

export const FETCH_ARTIST_FULL_START = "FETCH_ARTIST_FULL_START";
export const FETCH_ARTIST_FULL_SUCCESS = "FETCH_ARTIST_FULL_SUCCESS";
export const FETCH_ARTIST_FULL_ERROR = "FETCH_ARTIST_FULL_ERROR";
export const FETCH_ARTIST_ALBUMS_START = "FETCH_ARTIST_ALBUMS_START";
export const FETCH_ARTIST_ALBUMS_SUCCESS = "FETCH_ARTIST_ALBUMS_SUCCESS";
export const FETCH_ARTIST_ALBUMS_ERROR = "FETCH_ARTIST_ALBUMS_ERROR";
export const FETCH_ARTIST_TOP_TRACKS_START = "FETCH_ARTIST_TRACKS_START";
export const FETCH_ARTIST_TOP_TRACKS_SUCCESS = "FETCH_ARTIST_TOP_TRACKS_SUCCESS";
export const FETCH_ARTIST_TOP_TRACKS_ERROR = "FETCH_ARTIST_TOP_TRACKS_ERROR";
export const FETCH_ARTIST_RELATED_ARTISTS_START = "FETCH_ARTIST_RELATED_ARTISTS_START";
export const FETCH_ARTIST_RELATED_ARTISTS_SUCCESS =
  "FETCH_ARTIST_RELATED_ARTISTS_SUCCESS";
export const FETCH_ARTIST_RELATED_ARTISTS_ERROR = "FETCH_ARTIST_RELATED_ARTISTS_ERROR";

export const fetchArtistFull = artistId => (dispatch, getState, {requests, schema}) => {
  /*
    Artist data nested in other requests misses some details. For the artist in-depth view,
    we need to get the full artist, and simply overwrite the entity for that id.
  */
  const {token} = getState().app.token;
  dispatch({type: FETCH_ARTIST_FULL_START});

  return fetch(requests.artist.fetchArtistFull(token, artistId))
    .then(res => res.json())
    .then(res => {
      const data = normalize(res, schema.artist);
      dispatch(addEntities(ARTIST_STATE_KEY, data.entities.artists));
      dispatch({type: FETCH_ARTIST_FULL_SUCCESS});
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_ARTIST_FULL_ERROR});
    });
};

export const fetchArtistAlbums = (artistId, marketCode, limit) => (
  dispatch,
  getState,
  {requests, schema}
) => {
  const {token} = getState().app.token;

  dispatch({type: FETCH_ARTIST_ALBUMS_START});

  return fetch(requests.artist.fetchArtistAlbums(token, artistId, marketCode, limit))
    .then(res => res.json())
    .then(res => {
      const data = normalize(res.items, [schema.album]);
      dispatch(addEntities(ARTIST_STATE_KEY, data.entities.artists));
      dispatch(addEntities(ALBUM_STATE_KEY, data.entities.albums));
      dispatch({type: FETCH_ARTIST_ALBUMS_SUCCESS});
      dispatch(viewArtistAlbumIdsAdd(data.result));
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_ARTIST_ALBUMS_ERROR});
    });
};

export const fetchArtistTopTracks = (artistId, marketCode) => (
  dispatch,
  getState,
  {requests, schema}
) => {
  const {token} = getState().app.token;
  dispatch({type: FETCH_ARTIST_TOP_TRACKS_START});

  return fetch(requests.artist.fetchArtistTopTracks(token, artistId, marketCode))
    .then(res => res.json())
    .then(res => {
      const data = normalize(res.tracks, [schema.track]);
      dispatch(addEntities(ALBUM_STATE_KEY, data.entities.albums));
      dispatch(addEntities(ARTIST_STATE_KEY, data.entities.artists));
      dispatch(addEntities(TRACK_STATE_KEY, data.entities.tracks));
      dispatch(viewArtistTopTrackIdsAdd(data.result));
      dispatch({type: FETCH_ARTIST_TOP_TRACKS_SUCCESS});
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_ARTIST_TOP_TRACKS_ERROR});
    });
};

export const fetchArtistRelatedArtists = artistId => (
  dispatch,
  getState,
  {requests, schema}
) => {
  const {token} = getState().app.token;
  dispatch({type: FETCH_ARTIST_RELATED_ARTISTS_START});

  return fetch(requests.artist.fetchArtistRelatedArtists(token, artistId))
    .then(res => res.json())
    .then(res => {
      const data = normalize(res.artists, [schema.artist]);
      dispatch(addEntities(ARTIST_STATE_KEY, data.entities.artists));
      dispatch(viewArtistRelatedArtistIdsAdd(data.result));
      dispatch({type: FETCH_ARTIST_RELATED_ARTISTS_SUCCESS});
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: FETCH_ARTIST_RELATED_ARTISTS_ERROR
      });
    });
};
