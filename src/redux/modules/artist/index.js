import * as artistActions from "./actions";
import artistReducer from "./reducer";

export const STATE_KEY = "artist";
export const reducer = artistReducer;
export const actions = artistActions;
