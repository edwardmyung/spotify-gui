export const VIEW_PLAYLISTS_MINE_ADD = "VIEW_PLAYLISTS_MINE_ADD";
export const VIEW_PLAYLIST_MINE_UNFOLLOW = "VIEW_PLAYLIST_MINE_UNFOLLOW";
export const VIEW_PLAYLIST_TRACKS_ADD = "VIEW_PLAYLIST_TRACKS_ADD";
export const VIEW_PLAYLIST_TRACKS_CLEAR = "VIEW_PLAYLIST_TRACKS_CLEAR";

export const viewPlaylistsMineAdd = playlistIds => {
  return {
    type: VIEW_PLAYLISTS_MINE_ADD,
    payload: {playlistIds}
  };
};

export const viewPlaylistMineUnfollow = playlistId => {
  return {
    type: VIEW_PLAYLIST_MINE_UNFOLLOW,
    payload: {playlistId}
  };
};

export const viewPlaylistTracksClear = () => ({
  type: VIEW_PLAYLIST_TRACKS_CLEAR
});

export const viewPlaylistTracksAdd = trackIds => ({
  type: VIEW_PLAYLIST_TRACKS_ADD,
  payload: {trackIds}
});
