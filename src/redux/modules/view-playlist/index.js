import * as viewPlaylistActions from "./actions";
import viewPlaylistReducer from "./reducer";

export const STATE_KEY = "viewPlaylist";
export const reducer = viewPlaylistReducer;
export const actions = viewPlaylistActions;
