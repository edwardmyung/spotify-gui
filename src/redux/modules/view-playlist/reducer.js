import * as actions from "./actions";
import {uniq} from "lodash";

const defaultState = {
  playlistsMineIds: [],
  playlistTrackIds: []
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.VIEW_PLAYLISTS_MINE_ADD:
      return {
        ...state,
        playlistsMineIds: uniq([...state.playlistsMineIds, ...action.payload.playlistIds])
      };

    case actions.VIEW_PLAYLIST_MINE_UNFOLLOW:
      let index = state.playlistsMineIds.indexOf(action.payload.playlistId);

      return {
        ...state,
        playlistMineIds: state.playlistsMineIds.splice(index, 1)
      };

    case actions.VIEW_PLAYLIST_TRACKS_CLEAR:
      return {
        ...state,
        playlistTrackIds: []
      };

    case actions.VIEW_PLAYLIST_TRACKS_ADD:
      return {
        ...state,
        playlistTrackIds: [...state.playlistTrackIds, ...action.payload.trackIds]
      };

    default:
      return state;
  }
};
