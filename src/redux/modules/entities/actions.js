export const ADD_ENTITIES = "ADD_ENTITIES";

export const addEntities = (entityKey, entities) => {
  return {
    type: ADD_ENTITIES,
    payload: {
      entityKey,
      entities
    }
  };
};
