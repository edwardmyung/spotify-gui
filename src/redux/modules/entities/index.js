import * as entitiesActions from "./actions";
import entitiesReducer from "./reducer";

export const STATE_KEY = "entities";
export const reducer = entitiesReducer;
export const actions = entitiesActions;
