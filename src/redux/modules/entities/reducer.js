import {ADD_ENTITIES, ADD_ENTITY} from "./actions";
import {STATE_KEY as USERS_STATE_KEY} from "../user";
import {STATE_KEY as PLAYLISTS_STATE_KEY} from "../playlist";
import {STATE_KEY as TRACKS_STATE_KEY} from "../track";
import {STATE_KEY as ARTIST_STATE_KEY} from "../artist";

const defaultState = {
  [USERS_STATE_KEY]: {},
  [PLAYLISTS_STATE_KEY]: {},
  [TRACKS_STATE_KEY]: {},
  [ARTIST_STATE_KEY]: {}
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case ADD_ENTITIES:
      return {
        ...state,
        [action.payload.entityKey]: {
          ...state[action.payload.entityKey],
          ...action.payload.entities
        }
      };

    default:
      return state;
  }
};
