import {normalize} from "normalizr";
import {addEntities} from "../entities/actions";
import {
  viewPlaylistsMineAdd,
  viewPlaylistMineUnfollow,
  viewPlaylistTracksAdd,
  viewPlaylistTracksClear
} from "../view-playlist/actions";

import {STATE_KEY as PLAYLIST_STATE_KEY} from "./index";
import {STATE_KEY as USER_STATE_KEY} from "../user/index";
import {STATE_KEY as ALBUM_STATE_KEY} from "../album/index";
import {STATE_KEY as TRACK_STATE_KEY} from "../track/index";
import {STATE_KEY as ARTIST_STATE_KEY} from "../artist/index";

export const FETCH_PLAYLISTS_MINE_START = "FETCH_PLAYLISTS_MINE_START";
export const FETCH_PLAYLISTS_MINE_SUCCESS = "FETCH_PLAYLISTS_MINE_SUCCESS";
export const FETCH_PLAYLISTS_MINE_ERROR = "FETCH_PLAYLISTS_MINE_ERROR";
export const FETCH_PLAYLIST_START = "FETCH_PLAYLIST_START";
export const FETCH_PLAYLIST_SUCCESS = "FETCH_PLAYLIST_SUCCESS";
export const FETCH_PLAYLIST_ERROR = "FETCH_PLAYLIST_ERROR";
export const FETCH_PLAYLIST_TRACKS_START = "FETCH_PLAYLIST_TRACKS_START";
export const FETCH_PLAYLIST_TRACKS_SUCCESS = "FETCH_PLAYLIST_TRACKS_SUCCESS";
export const FETCH_PLAYLIST_TRACKS_ERROR = "FETCH_PLAYLIST_TRACKS_ERROR";
export const UNFOLLOW_PLAYLIST_START = "UNFOLLOW_PLAYLIST_START";
export const UNFOLLOW_PLAYLIST_SUCCESS = "UNFOLLOW_PLAYLIST_SUCCESS";
export const UNFOLLOW_PLAYLIST_ERROR = "UNFOLLOW_PLAYLIST_ERROR";

export const fetchPlaylistsMine = (limit = 10, offset) => (
  dispatch,
  getState,
  {requests, schema}
) => {
  const {token} = getState().app.token;

  dispatch({type: FETCH_PLAYLISTS_MINE_START});

  return fetch(requests.playlist.fetchPlaylistsMine(token, limit, offset))
    .then(res => {
      if (res.statusText === "Unauthorized") {
        window.location.href = "./";
      }
      return res.json();
    })
    .then(res => {
      const data = normalize(res.items, [schema.playlist]);
      dispatch(addEntities(PLAYLIST_STATE_KEY, data.entities.playlists));
      dispatch(addEntities(USER_STATE_KEY, data.entities.users));
      dispatch(viewPlaylistsMineAdd(data.result));
      dispatch({type: FETCH_PLAYLISTS_MINE_SUCCESS});
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_PLAYLISTS_MINE_ERROR});
    });
};

export const fetchPlaylistTracks = (playlistId, limit = 100, offset = 0) => (
  dispatch,
  getState,
  {requests, schema}
) => {
  let {token} = getState().app.token;
  dispatch({type: FETCH_PLAYLIST_TRACKS_START});

  return fetch(requests.playlist.fetchPlaylistTracks(token, playlistId, limit, offset))
    .then(res => {
      return res.json();
    })
    .then(res => {
      const tracks = res.items.map(wrappedTrack => wrappedTrack.track);
      const data = normalize(tracks, [schema.track]);

      dispatch(addEntities(TRACK_STATE_KEY, data.entities.tracks));
      dispatch(addEntities(ALBUM_STATE_KEY, data.entities.albums));
      dispatch(addEntities(ARTIST_STATE_KEY, data.entities.artists));
      if (offset === 0) {
        dispatch(viewPlaylistTracksClear());
      }
      dispatch(viewPlaylistTracksAdd(data.result));
      dispatch({type: FETCH_PLAYLIST_TRACKS_SUCCESS});
    })
    .catch(err => {
      console.log(err);
      dispatch({type: FETCH_PLAYLIST_TRACKS_ERROR});
    });
};

export const createNewPlaylist = (playlistName, isPublic) => (
  dispatch,
  getState,
  {requests, schema}
) => {
  const {token} = getState().app.token;
  const {currentUserId} = getState().app.user;
  dispatch({type: FETCH_PLAYLIST_START});

  return fetch(
    requests.playlist.createNewPlaylist(token, currentUserId, playlistName, isPublic)
  )
    .then(res => res.json())
    .then(playlist => {
      dispatch({type: FETCH_PLAYLIST_SUCCESS});
      const data = normalize(playlist, schema.playlist);
      dispatch(addEntities(PLAYLIST_STATE_KEY, data.entities.playlists));
      dispatch(addEntities(USER_STATE_KEY, data.entities.users));
      dispatch(viewPlaylistsMineAdd([data.result]));
    })
    .catch(err => {
      dispatch({type: FETCH_PLAYLIST_ERROR});
      console.log(err);
    });
};

/*
  Spotify's playlist system does not allow deletion of playlists, only unfollowing...
  This is to allow others following it to continue enjoying the playlist.
*/
export const unfollowPlaylist = playlistId => (dispatch, getState, {requests}) => {
  const {token} = getState().app.token;
  dispatch({type: UNFOLLOW_PLAYLIST_START});

  return fetch(requests.playlist.unfollowPlaylist(token, playlistId))
    .then(_res => {
      dispatch({type: UNFOLLOW_PLAYLIST_SUCCESS});
      dispatch(viewPlaylistMineUnfollow(playlistId));
    })
    .catch(err => {
      console.log(err);
    });
};
