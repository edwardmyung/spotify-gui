import * as playlistActions from "./actions";
import playlistReducer from "./reducer";

export const actions = playlistActions;
export const reducer = playlistReducer;
export const STATE_KEY = "playlist";
