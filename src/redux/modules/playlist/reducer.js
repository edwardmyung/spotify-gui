import * as actions from "./actions";

const defaultState = {
  playlistTracksLoading: true,
  playlistsMineLoading: true
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.FETCH_PLAYLISTS_MINE_START:
      return {
        ...state,
        playlistsMineLoading: true
      };

    case actions.FETCH_PLAYLISTS_MINE_SUCCESS:
    case actions.FETCH_PLAYLISTS_MINE_ERROR:
      return {
        ...state,
        playlistsMineLoading: false
      };

    case actions.FETCH_PLAYLIST_TRACKS_START:
      return {
        ...state,
        playlistTracksLoading: true
      };

    case actions.FETCH_PLAYLIST_TRACKS_SUCCESS:
    case actions.FETCH_PLAYLIST_TRACKS_ERROR:
      return {
        ...state,
        playlistTracksLoading: false
      };

    default:
      return state;
  }
};
