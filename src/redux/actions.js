import {actions as tokenActions} from "./modules/token";
import {actions as albumActions} from "./modules/album";
import {actions as playlistActions} from "./modules/playlist";
import {actions as artistActions} from "./modules/artist";
import {actions as userActions} from "./modules/user";
import {actions as browseActions} from "./modules/browse";
import {actions as searchActions} from "./modules/search";

import {actions as viewPlayerActions} from "./modules/view-player";
import {actions as viewArtistActions} from "./modules/view-artist";
import {actions as viewAlbumActions} from "./modules/view-album";
import {actions as viewCollectionActions} from "./modules/view-collection";
import {actions as viewBrowseActions} from "./modules/view-browse";
import {actions as viewSearchActions} from "./modules/view-search";

export const actions = {
  token: tokenActions,
  playlist: playlistActions,
  artist: artistActions,
  album: albumActions,
  user: userActions,
  browse: browseActions,
  search: searchActions,

  viewArtist: viewArtistActions,
  viewPlayer: viewPlayerActions,
  viewAlbum: viewAlbumActions,
  viewCollection: viewCollectionActions,
  viewBrowse: viewBrowseActions,
  viewSearch: viewSearchActions
};
