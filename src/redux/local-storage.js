import constants from "../constants";

export const loadState = () => {
  /*
    If this function:
    - returns an object => createStore will set this as the initial state
    - returns undefined => createStore will run reducers to create state instead
  */
  try {
    const serializedState = localStorage.getItem(constants.localStorage.reduxStore);
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch {
    return undefined;
  }
};

export const saveState = state => {
  /*
    This function is run on store.subscribe(), saving state to localStorage whenever the 
    store is updated
  */
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem(constants.localStorage.reduxState, serializedState);
  } catch (err) {
    console.log(err);
  }
};
