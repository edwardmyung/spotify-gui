import {compose, createStore, applyMiddleware} from "redux";
import reducer from "./reducer";
import thunk from "redux-thunk";
import {requests, schema} from "../api";
import {loadState, saveState} from "./local-storage";

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const preLoadedState = loadState();

const store = createStore(
  reducer,
  preLoadedState,
  composeEnhancer(applyMiddleware(thunk.withExtraArgument({requests, schema})))
);

store.subscribe(() => {
  saveState(store.getState());
});

export default store;
