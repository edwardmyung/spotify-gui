import {combineReducers} from "redux";

import {reducer as token, STATE_KEY as TOKEN_STATE_KEY} from "./modules/token";
import {reducer as playlist, STATE_KEY as PLAYLIST_STATE_KEY} from "./modules/playlist";
import {reducer as entities} from "./modules/entities";
import {reducer as user, STATE_KEY as USER_STATE_KEY} from "./modules/user";
import {reducer as artist, STATE_KEY as ARTIST_STATE_KEY} from "./modules/artist";
import {reducer as album, STATE_KEY as ALBUM_STATE_KEY} from "./modules/album";
import {reducer as browse, STATE_KEY as BROWSE_STATE_KEY} from "./modules/browse";
import {reducer as search, STATE_KEY as SEARCH_STATE_KEY} from "./modules/search";

import {reducer as viewPlaylist, STATE_KEY as VIEW_PLAYLIST_STATE_KEY} from "./modules/view-playlist";
import {reducer as viewPlayer, STATE_KEY as VIEW_PLAYER_STATE_KEY} from "./modules/view-player";
import {reducer as viewArtist, STATE_KEY as VIEW_ARTIST_STATE_KEY} from "./modules/view-artist";
import {reducer as viewAlbum, STATE_KEY as VIEW_ALBUM_STATE_KEY} from "./modules/view-album";
import {reducer as viewCollection, STATE_KEY as VIEW_COLLECTION_STATE_KEY} from "./modules/view-collection";
import {reducer as viewBrowse, STATE_KEY as VIEW_BROWSE_STATE_KEY} from "./modules/view-browse";
import {reducer as viewSearch, STATE_KEY as VIEW_SEARCH_STATE_KEY} from "./modules/view-search";

const app = combineReducers({
  [TOKEN_STATE_KEY]: token,
  [PLAYLIST_STATE_KEY]: playlist,
  [ARTIST_STATE_KEY]: artist,
  [USER_STATE_KEY]: user,
  [ALBUM_STATE_KEY]: album,
  [BROWSE_STATE_KEY]: browse,
  [SEARCH_STATE_KEY]: search,

  [VIEW_PLAYLIST_STATE_KEY]: viewPlaylist,
  [VIEW_PLAYER_STATE_KEY]: viewPlayer,
  [VIEW_ARTIST_STATE_KEY]: viewArtist,
  [VIEW_ALBUM_STATE_KEY]: viewAlbum,
  [VIEW_COLLECTION_STATE_KEY]: viewCollection,
  [VIEW_BROWSE_STATE_KEY]: viewBrowse,
  [VIEW_SEARCH_STATE_KEY]: viewSearch
});

const reducer = combineReducers({
  app,
  entities
});

export default reducer;
