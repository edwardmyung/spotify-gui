import * as viewConstants from "./view";
import * as localStorageConstants from "./local-storage";

export default {
  view: viewConstants,
  localStorage: localStorageConstants
};
