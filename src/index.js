import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {ThemeProvider} from "styled-components";
import styleGuidelines from "./style-guidelines";
import store from "./redux";
import Router from "./router";

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={styleGuidelines}>
      <Router />
    </ThemeProvider>
  </Provider>,
  document.getElementById("root")
);

module.hot.accept();
