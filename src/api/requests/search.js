export const fetchResults = (token, limit, offset, query) => {
  const sanitizedQuery = encodeURI(query);
  const types = ["album", "artist", "playlist", "track"].join(",");

  return new Request(
    `https://api.spotify.com/v1/search?q=${sanitizedQuery}&type=${types}&limit=${limit}&offset=${offset}`,
    {
      headers: new Headers({
        Authorization: `Bearer ${token}`
      })
    }
  );
};
