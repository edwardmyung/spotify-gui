export const fetchPlaylistsMine = (token, limit = 50, offset = 0) => {
  return new Request(
    `https://api.spotify.com/v1/me/playlists?limit=${limit}&offset=${offset}`,
    {
      headers: new Headers({
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      })
    }
  );
};

export const fetchPlaylistTracks = (token, playlistId, limit = 100, offset = 0) => {
  return new Request(
    `https://api.spotify.com/v1/playlists/${playlistId}/tracks?limit=${limit}&offset=${offset}`,
    {
      headers: new Headers({
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      })
    }
  );
};

export const createNewPlaylist = (token, userId, name, isPublic = false) => {
  return new Request(`https://api.spotify.com/v1/users/${userId}/playlists?`, {
    method: "POST",
    headers: new Headers({
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json"
    }),
    body: JSON.stringify({
      name,
      public: isPublic
    })
  });
};

export const unfollowPlaylist = (token, playlistId) => {
  return new Request(`https://api.spotify.com/v1/playlists/${playlistId}/followers`, {
    method: "DELETE",
    headers: new Headers({
      Authorization: `Bearer ${token}`
    })
  });
};
