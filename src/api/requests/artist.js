export const fetchArtistFull = (token, artistId) => {
  return new Request(`https://api.spotify.com/v1/artists/${artistId}`, {
    headers: new Headers({
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json"
    })
  });
};

export const fetchArtistAlbums = (token, artistId, marketCode, limit = 4) => {
  return new Request(
    `https://api.spotify.com/v1/artists/${artistId}/albums?market=${marketCode}&include_groups=appears_on&limit=${limit}`,
    {
      headers: new Headers({
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      })
    }
  );
};

export const fetchArtistTopTracks = (token, artistId, marketCode) => {
  return new Request(
    `https://api.spotify.com/v1/artists/${artistId}/top-tracks?country=${marketCode}`,
    {
      headers: new Headers({
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      })
    }
  );
};

export const fetchArtistRelatedArtists = (token, artistId) => {
  return new Request(`https://api.spotify.com/v1/artists/${artistId}/related-artists`, {
    headers: new Headers({
      Authorization: `Bearer ${token}`
    })
  });
};
