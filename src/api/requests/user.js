export const fetchCurrentUser = token => {
  return new Request(`https://api.spotify.com/v1/me`, {
    headers: new Headers({
      Authorization: `Bearer ${token}`
    })
  });
};

export const fetchCurrentUserTopArtists = (token, limit, offset) => {
  return new Request(
    `https://api.spotify.com/v1/me/top/artists?limit=${limit}&offset=${offset}`,
    {
      headers: new Headers({
        Authorization: `Bearer ${token}`
      })
    }
  );
};

export const fetchCurrentUserTopTracks = (token, limit, offset) => {
  return new Request(
    `https://api.spotify.com/v1/me/top/tracks?limit=${limit}&offset=${offset}`,
    {
      headers: new Headers({
        Authorization: `Bearer ${token}`
      })
    }
  );
};
