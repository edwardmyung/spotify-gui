import * as playlist from "./playlist";
import * as artist from "./artist";
import * as user from "./user";
import * as album from "./album";
import * as browse from "./browse";
import * as search from "./search";

export default {
  playlist,
  artist,
  user,
  album,
  browse,
  search
};
