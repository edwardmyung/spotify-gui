export const fetchAlbumTracks = (token, albumId, limit, offset) => {
  return new Request(
    `https://api.spotify.com/v1/albums/${albumId}/tracks?limit=${limit}&offset=${offset}`,
    {
      headers: new Headers({
        Authorization: `Bearer ${token}`
      })
    }
  );
};
