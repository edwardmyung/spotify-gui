export const fetchBrowseGenreSeeds = token => {
  return new Request(`https://api.spotify.com/v1/recommendations/available-genre-seeds`, {
    headers: new Headers({
      Authorization: `Bearer ${token}`
    })
  });
};

export const fetchBrowseRecommendations = (token, limit, market, genreSeeds, targets, key) => {
  const genreSeedsString = genreSeeds.join(",");

  let targetString = "";
  if (Object.keys(targets).length > 0) {
    Object.keys(targets).forEach(key => {
      targetString += `&${key}=${targets[key]}`;
    });
  }

  return new Request(
    `https://api.spotify.com/v1/recommendations?seed_genres=${genreSeedsString}&market=${market}&limit=${limit}${targetString}&key=${key}`,
    {
      headers: new Headers({
        Authorization: `Bearer ${token}`
      })
    }
  );
};

export const fetchBrowseCategories = (token, limit) => {
  return new Request(`https://api.spotify.com/v1/browse/categories?limit=${limit}`, {
    headers: new Headers({
      Authorization: `Bearer ${token}`
    })
  });
};

export const fetchBrowseCategoriesPlaylists = (token, limit, offset, categoryId) => {
  return new Request(
    `https://api.spotify.com/v1/browse/categories/${categoryId}/playlists?limit=${limit}&offset=${offset}`,
    {
      headers: new Headers({
        Authorization: `Bearer ${token}`
      })
    }
  );
};

export const fetchBrowseNewReleaseAlbums = (token, limit, offset) => {
  return new Request(`https://api.spotify.com/v1/browse/new-releases?limit=${limit}&offset=${offset}`, {
    headers: new Headers({
      Authorization: `Bearer ${token}`
    })
  });
};
