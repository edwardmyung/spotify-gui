import {schema} from "normalizr";

export const user = new schema.Entity("users");

export const artist = new schema.Entity("artists");

export const album = new schema.Entity("albums", {
  artists: [artist]
});

export const track = new schema.Entity("tracks", {
  artists: [artist],
  album: album
});

export const playlist = new schema.Entity("playlists", {
  owner: user
});
