import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {actions} from "../redux/actions";
import globalStyle from "../components/style";
const MARKET_CODE = "GB";

class Artist extends React.Component {
  componentDidMount() {
    const {artistId} = this.props;

    /*
      This promise fetches the incomplete, nested artist resource, after which the full 
      artist is fetched
    */ 
    const promise = new Promise((resolve, _reject) => {
      this.props.fetchArtistAlbums(artistId, MARKET_CODE, 6);
      this.props.fetchArtistTopTracks(artistId, MARKET_CODE);
      this.props.fetchArtistRelatedArtists(artistId);
      resolve();
    });

    promise.then(function(){
      this.props.fetchArtistFull(artistId);
    }.bind(this));
  }

  render() {
    const {isLoading, artist, albums, topTracks, relatedArtists} = this.props;

    let body;

    if(isLoading) {
      body = <div>LOADING</div>;
    } else {
      body = (
        <React.Fragment>
          <div>Artist with name {artist.name}</div>
          {artist.followers && artist.followers.total}  

          <br/>
          <h4>Top albums</h4>
          {albums.map(x => <span key={x.id}>{x.id}<br/></span>)}
          <br/>

          <h4>Top tracks</h4>
          {topTracks.map(x => <span key={x.id}>{x.id}<br/></span>)}

          <h4>Related artists</h4>
          {relatedArtists.map(x => <span key={x.id}>{x.id}<br/></span>)}
        </React.Fragment>
      );
    }
    
    return (
      <globalStyle.layout.BodyContent>
        <globalStyle.layout.Container>
          {body}
        </globalStyle.layout.Container>
      </globalStyle.layout.BodyContent>
    )
  }
} 

const mapStateToProps = (state, ownProps) => {
  const {match} = ownProps;
  const {params : {artistId}} = match;

  const artist = state.entities.artist[artistId];
  const albums = state.app.viewArtist.albumIds.map(albumId => state.entities.album[albumId]);
  const topTracks = state.app.viewArtist.topTrackIds.map(trackId => state.entities.track[trackId]);
  const relatedArtists = state.app.viewArtist.relatedArtistIds.map(artistId => state.entities.artist[artistId]);
  
  return {
    artistId,
    artist,
    albums,
    topTracks,
    relatedArtists,
    isLoading : state.app.artist.artistFullLoading   // TODO: make sure this is the right loading state
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchArtistFull : actions.artist.fetchArtistFull,
  fetchArtistAlbums : actions.artist.fetchArtistAlbums,
  fetchArtistTopTracks : actions.artist.fetchArtistTopTracks,
  fetchArtistRelatedArtists : actions.artist.fetchArtistRelatedArtists
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Artist);
