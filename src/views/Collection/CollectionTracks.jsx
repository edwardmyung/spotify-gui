import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {actions} from "../../redux/actions";
import {Track} from "../../components";

class Component extends React.Component {
  componentDidMount() {
    const {fetchCurrentUserTopTracks} = this.props;
    fetchCurrentUserTopTracks(20,0);
  }
  
  render() {
    const {isLoading, topTracks} = this.props;

    if(isLoading) {
      return <div>Loading</div>
    }

    return (
      <div>
        <h4>Top Tracks</h4>
        {topTracks.map(track => <Track key={track.id} track={track}/>)}
      </div>
    )
  }
}

const mapStateToProps = state => {
  const isLoading = state.app.user.currentUserTopTracksLoading;
  const topTracks = state.app.viewCollection.topTrackIds.map(trackId => state.entities.track[trackId]);
  return {
    isLoading,
    topTracks,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchCurrentUserTopTracks : actions.user.fetchCurrentUserTopTracks
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Component);