import React from "react";
import globalStyle from "../../components/style";
import {Tabs} from "../../components";
import {Switch, Route} from "react-router-dom";

import CollectionArtists from "./CollectionArtists";
import CollectionTracks from "./CollectionTracks";

export default ({ match }) => {
  const tabData = [
    { linkTo : `${match.url}/tracks`, label : "Top Tracks" },
    { linkTo : `${match.url}/artists`, label : "Top Artists" }
  ];

  return (
    <globalStyle.layout.BodyContent>
      <globalStyle.layout.Container>
        <Tabs tabData={tabData} />
        
        <Switch>
          <Route exact path={`${match.path}/tracks`} component={CollectionTracks} />
          <Route exact path={`${match.path}/artists`} component={CollectionArtists}/>
        </Switch>
      </globalStyle.layout.Container>
    </globalStyle.layout.BodyContent>
  );
};