import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {actions} from "../../redux/actions";

class Component extends React.Component {
  componentDidMount() {
    const {fetchCurrentUserTopArtists} = this.props;
    fetchCurrentUserTopArtists(20, 0);
  }

  render() {
    const {isLoading, topArtists} = this.props;
    
    if(isLoading) {
      return <div>Loading</div>;
    }
    
    return (
      <div>
        <h4>Top artists</h4>
        {topArtists.map(topArtist => <div key={topArtist.id}>{topArtist.id}</div>)}
      </div>
    )
  }
}


const mapStateToProps = state => {
  const isLoading = state.app.user.currentUserTopArtistsLoading;
  const topArtists = state.app.viewCollection.topArtistIds.map(artistId => state.entities.artist[artistId]);
  return {
    isLoading,
    topArtists,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchCurrentUserTopArtists : actions.user.fetchCurrentUserTopArtists,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Component);