import React from "react";

export default ({albums, artists, playlists, tracks}) => {
  const renderedPlaylists = playlists.length > 0 && (
    <div>
      <h3>Playlists</h3>
      {playlists.map(playlist => <div key={playlist.id}>{playlist.name}</div>)}
    </div>
  );

  const renderedTracks = tracks.length > 0 && (
    <div>
      <h3>Tracks</h3>
      {tracks.map(track => <div key={track.id}>{track.name}</div>)}
    </div>
  );

  const renderedArtists = artists.length > 0 && (
    <div>
      <h3>Artists</h3>
      {artists.map(artist => <div key={artist.id}>{artist.name}</div>)}
    </div>
  );

  const renderedAlbums = albums.length > 0 && (
    <div>
      <h3>Albums</h3>
      {albums.map(album => <div key={album.id}>{album.name}</div>)}
    </div>
  );

  return (
    <div>
      {renderedPlaylists}
      {renderedTracks}
      {renderedArtists}
      {renderedAlbums}
    </div>
  );
};