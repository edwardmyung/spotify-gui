import React, {useEffect, useState} from "react";
import globalStyle from "../../components/style";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {actions} from "../../redux/actions";
import Results from "./results";

const Component = (props) => {
  const [searchInput, setSearchInput] = useState("");
  const {clearSearchResults, fetchSearchResults, albums, artists, playlists, tracks, isEmpty} = props;

  useEffect(() => {
    // ensure search starts anew each time this view loads
    clearSearchResults();
  }, []);
  
  const search = searchInput => () => {
    fetchSearchResults(6, 0, searchInput);
  }

  const renderEmptyPlaceholder = () => (
    <div>
      <h3>Search Spotify</h3>
      Find your favorite songs, artists, albums, podcasts and playlists.
    </div>
  );

  return (
    <globalStyle.layout.BodyContent>
      <globalStyle.layout.Container>
        <input type="text" value={searchInput} onChange={ e => setSearchInput(e.target.value) } />
        <button onClick={ search(searchInput) }>Search</button>

        {isEmpty ? renderEmptyPlaceholder() : <Results artists={artists} tracks={tracks} playlists={playlists} albums={albums}/> }
      </globalStyle.layout.Container>
    </globalStyle.layout.BodyContent>
  )
}

const mapStateToProps = state => {
  const artists = state.app.viewSearch.artistIds.map(artistId => state.entities.artist[artistId]);
  const tracks = state.app.viewSearch.trackIds.map(trackId => state.entities.track[trackId]);
  const playlists = state.app.viewSearch.playlistIds.map(playlistId => state.entities.playlist[playlistId]);
  const albums = state.app.viewSearch.albumIds.map(albumId => state.entities.album[albumId]);
  return {
    artists,
    tracks,
    playlists,
    albums,
    isEmpty : artists.length + tracks.length + playlists.length + albums.length === 0
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  clearSearchResults : actions.viewSearch.viewSearchClearAllIds,
  fetchSearchResults : actions.search.fetchResults,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Component);