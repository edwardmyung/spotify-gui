import React from "react";
import globalStyle from "../components/style";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {actions} from "../redux/actions";

class Component extends React.Component {
  componentDidMount() {
    const {
      match,
      fetchAlbumTracks
    } = this.props;
    
    fetchAlbumTracks(match.params.albumId, 50, 0);
  }

  render() {
    const {match, tracks, isLoading} = this.props;
    
    let body;

    if(isLoading) {
      body = <div>Loading</div>
    } else {
      body = (
        <div>
          Album with id {match.params.albumId}
          <h4>Tracks</h4>
          {tracks.map(track => <div key={track.id}>{track.name}</div>)}
        </div>
      );
    }

    return (
      <globalStyle.layout.BodyContent>
        <globalStyle.layout.Container>
          {body}
        </globalStyle.layout.Container>
      </globalStyle.layout.BodyContent>
    );
  }
} 

const mapStateToProps = state => {
  const tracks = state.app.viewAlbum.trackIds.map( trackId => state.entities.track[trackId] );

  return {
    isLoading: state.app.album.albumTracksLoading,
    tracks
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchAlbumTracks : actions.album.fetchAlbumTracks
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Component);