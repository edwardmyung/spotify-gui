import React from "react";
import globalStyle from "../components/style";

export default ({match}) => {
  const { params : {userId}} = match;
  return (
    <globalStyle.layout.BodyContent>
      <globalStyle.layout.Container>
        User with id {userId}
      </globalStyle.layout.Container>
    </globalStyle.layout.BodyContent>
  )
}