import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Track, Button} from "../../components";
import {Link} from "react-router-dom";

import * as localStyle from "./style";
import globalStyle from "../../components/style";
import {actions} from "../../redux/actions";
import {debounce} from "lodash";

const PAGE_SIZE = 100;

const PlaylistMetaInfo = ({playlist, owner, firstTrack}) => {
  const onClickPlayPlaylist = () => {
    window.playerComponent.setTrack(playlist.uri, firstTrack.uri);
  };
    
  return (
    <localStyle.WrapperPlaylistMetaInfo>
      <localStyle.WrapperPlaylistMetaInfoImageWrapper>
        <localStyle.WrapperPlaylistMetaInfoImage src={playlist.images[0] && playlist.images[0].url}/>
      </localStyle.WrapperPlaylistMetaInfoImageWrapper>

      <localStyle.PlaylistMetaInfoName>
        { playlist.name }
      </localStyle.PlaylistMetaInfoName>
      <Link to={`/user/${owner.id}`}>
        <localStyle.PlaylistMetaInfoOwner>
          { owner.display_name }
        </localStyle.PlaylistMetaInfoOwner>
      </Link>
      {playlist.tracks.total} songs
      <br/>
      <Button 
        type="brand" 
        onClick={onClickPlayPlaylist}
        style={{ marginTop : 10 }}>
        Play
      </Button>
    </localStyle.WrapperPlaylistMetaInfo>
  );
}

class Playlist extends React.Component {
  constructor() {
    super();

    this.wrapperRef = React.createRef();

    this.state = {
      page : 0
    };
    this.onScroll = this.onScroll.bind(this);
  }

  componentDidMount() {
    const {match, fetchPlaylistTracks} = this.props;
    fetchPlaylistTracks(match.params.playlistId, PAGE_SIZE, this.state.page * PAGE_SIZE); 
  }

  componentDidUpdate(prevProps, _prevState, _snapshot) {
    const {match, fetchPlaylistTracks} = this.props;
    if(prevProps.match.params.playlistId !== match.params.playlistId) {
      this.wrapperRef.current.scrollTop = 0;
      this.setState({page : 0}, () => {
        fetchPlaylistTracks(match.params.playlistId, PAGE_SIZE, this.state.page * PAGE_SIZE); 
      });
    }
  }

  onScroll() {
    // console.log("fetching more");
    const {match, fetchPlaylistTracks} = this.props;
    const { scrollTop, offsetHeight, scrollHeight } = this.wrapperRef.current;

    if(offsetHeight + scrollTop > scrollHeight - 100) {
      this.setState({page: this.state.page + 1 }, () => {
        fetchPlaylistTracks(match.params.playlistId, PAGE_SIZE, this.state.page * PAGE_SIZE);
      });
    }
  }

  render() {
    const { isLoading, tracks, playlist, owner, currentTrackId } = this.props;
    
    let body;

    if(isLoading) {
      body = <div>LOADING</div>
    } else if (playlist && owner) {
      body = (
        <localStyle.Wrapper>
          <PlaylistMetaInfo playlist={playlist} owner={owner} firstTrack={tracks[0]}/>
          <localStyle.WrapperPlaylistTracks>
            {
              tracks.map(track => {
                return track && 
                  <Track 
                    key={track.id} 
                    track={track} 
                    playlistId={playlist.id}/>
              } )
            }
          </localStyle.WrapperPlaylistTracks>
        </localStyle.Wrapper>
      );
    } 
    
    return (
      <globalStyle.layout.BodyContent 
        ref={this.wrapperRef}
        onScroll={ debounce(this.onScroll, 400) }>
        <globalStyle.layout.Container>
          {body}
        </globalStyle.layout.Container>
      </globalStyle.layout.BodyContent>
    );
  }
} 

const mapStateToProps = (state, ownProps) => {
  const playlist = state.entities.playlist[ownProps.match.params.playlistId]
  
  return {
    tracks : state.app.viewPlaylist.playlistTrackIds.map(id => state.entities.track[id]),
    isLoading : state.app.playlist.playlistTracksLoading,
    playlist,
    owner : state.entities.user[playlist && playlist.owner]
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchPlaylistTracks: actions.playlist.fetchPlaylistTracks,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Playlist);
