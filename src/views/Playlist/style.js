import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
`;

export const WrapperPlaylistMetaInfo = styled.div`
  width: 300px;
  flex-shrink: 0;
  padding: 10px;
  text-align: center;
`;

export const WrapperPlaylistTracks = styled.div`
  flex-grow: 1;
  overflow: hidden;
`;

export const WrapperPlaylistMetaInfoImageWrapper = styled.div`
  width: 100%;
  padding-top: 100%;
  overflow: hidden;
  position: relative;
  margin-bottom: 12px;
`;

export const WrapperPlaylistMetaInfoImage = styled.img.attrs(p => ({
  src: p.imageUrl
}))`
  position: absolute;
  left: 50%;
  top: 50%;
  width: 100%;
  transform: translate(-50%, -50%);
`;

export const PlaylistMetaInfoName = styled.div`
  ${p => p.theme.typography.primary.h2.bold};
`;

export const PlaylistMetaInfoOwner = styled.div`
  ${p => p.theme.typography.primary.p.regular};
  color: ${p => p.theme.colors.greys.tone4};
`;
