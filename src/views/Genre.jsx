import React, {useEffect} from "react";
import globalStyle from "../components/style";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import { actions } from "../redux/actions";
import {Link} from "react-router-dom";

const Component = (props) => {
  const {
    fetchBrowseCategoriesPlaylists,
    playlists,
    isLoading
  } = props;

  useEffect(() => {
    fetchBrowseCategoriesPlaylists(props.match.params.genreCategoryId, 50, 0);
  }, [props.match.params]);


  let body;
  
  if(isLoading) {
    body = <div>LOADING</div>
  } else {
    body = (
      <React.Fragment>
        <h4>Genre playlists</h4>
        {playlists.map(playlist => (
          <Link key={playlist.id} to={`/playlist/${playlist.id}`}>
            <div>{playlist.name}</div>
          </Link>
        ))}
      </React.Fragment>
    )
  }

  return (
    <globalStyle.layout.BodyContent>
      <globalStyle.layout.Container>
        {body}
      </globalStyle.layout.Container>
    </globalStyle.layout.BodyContent>
  )
}

const mapStateToProps = state => {
  const playlists = state.app.viewBrowse.genrePlaylistIds.map(playlistId => state.entities.playlist[playlistId]);
  return {
    playlists,
    isLoading : state.app.browse.genrePlaylistsLoading,
  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchBrowseCategoriesPlaylists : actions.browse.fetchBrowseCategoriesPlaylists
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Component);