import React from "react";
import * as localStyle from "./style";

export default ({step}) => {
  return (
    <div style={{ flexBasis : "220px", flexShrink : 0 }}>
      <localStyle.DiscoverStepWrapper active={step === 0}>
        1. Select Genres
        {
          step === 0 && 
            <localStyle.DiscoverStepWrapperBlurb>
              Select up to 5 genres
            </localStyle.DiscoverStepWrapperBlurb>
        }
      </localStyle.DiscoverStepWrapper>
      <localStyle.DiscoverStepWrapper active={step === 1}>
        2. Set Preferences
        {
          step === 1 && 
            <localStyle.DiscoverStepWrapperBlurb>
              Describe what you're looking for (Optional)
            </localStyle.DiscoverStepWrapperBlurb>
        }
      </localStyle.DiscoverStepWrapper>
      <localStyle.DiscoverStepWrapper active={step === 2}>
        3. Results
        {
          step === 2 && 
            <localStyle.DiscoverStepWrapperBlurb>
              Listen to your new mix
            </localStyle.DiscoverStepWrapperBlurb>
        }
      </localStyle.DiscoverStepWrapper>
    </div>
  );
}