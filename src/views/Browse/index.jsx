import React from "react";
import {style as globalStyle, Tabs} from "../../components";
import {Route, Switch} from "react-router-dom";

import Discover from "./Discover";
import NewReleases from "./NewReleases";
import Genres from "./Genres";

export default ({match}) => {
  const tabData = [
    { linkTo : `${match.url}/discover`, label : "Discover" },
    { linkTo : `${match.url}/genres`, label : "Genres" },
    { linkTo : `${match.url}/new-releases`, label : "New releases" }
  ];
  
  return (
    <globalStyle.layout.BodyContent>
      <globalStyle.layout.Container>
        <Tabs tabData={tabData}/>

        <Switch>
          <Route path={`${match.path}/discover`} component={Discover}/>
          <Route path={`${match.path}/new-releases`} component={NewReleases}/>
          <Route path={`${match.path}/genres`} component={Genres}/>
        </Switch>
      </globalStyle.layout.Container>
    </globalStyle.layout.BodyContent>
  );
};