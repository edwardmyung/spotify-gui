import React, {useEffect} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import { actions } from "../../redux/actions";
import {Link} from "react-router-dom";

const Component = (props) => {
  const {
    fetchBrowseNewReleaseAlbums,
    albums
  } = props;

  useEffect(() => {
    fetchBrowseNewReleaseAlbums(20, 0);
  })

  return (
    <div>
      <h1>New Releases</h1>
      {albums.map(album => (
        <Link 
          key={album.id}
          to={`/album/${album.id}`}>
          <div>{album.name}</div>
        </Link>
      ))}
    </div>
  );
}

const mapStateToProps = state => {
  const albums = state.app.viewBrowse.newReleaseAlbumIds.map( albumId => {
    return state.entities.album[albumId];
  } )
  return {
    albums 
  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchBrowseNewReleaseAlbums: actions.browse.fetchBrowseNewReleaseAlbums
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Component);