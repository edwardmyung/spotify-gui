import React from "react";

export default ({value, updateParent, stateKey, fetchBrowseRecommendations}) => {
  const onChange = e => {
    // note the event object here holds the value as a string, need to convert to integer
    updateParent({[stateKey]: parseInt(e.target.value)});
    fetchBrowseRecommendations();
  };

  const keys = [
    "C", "C#", "D", "E♭", "E", "F", "F#", "G", "A♭", "A", "B♭", "B"
  ]

  return (
    <div>
      <div>Key</div>

      {keys.map((key, ix) => (
        <label key={key}>
          <input type="radio" value={ix} checked={value===ix} onChange={onChange}/>
          {key}
        </label>
      ))}
    
      <div onClick={() => updateParent({[stateKey] : null})}>X</div>
    </div>
  );
};
