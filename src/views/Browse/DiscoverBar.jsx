import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {fromEvent} from "rxjs";
import {takeUntil, mergeMap} from"rxjs/operators";
import * as localStyle from "./style";

class Component extends React.Component {
  constructor() {
    super();
    this.outerBarRef = React.createRef();
    this.innerBarRef = React.createRef();
    this.state = {
      leftPct : 0
    };
  }
  
  componentDidMount() {
    const move$ = fromEvent(document, "mousemove");
    const down$ = fromEvent(this.outerBarRef.current, "mousedown");
    const up$ = fromEvent(document, "mouseup");

    const drag$ = down$.pipe(mergeMap(_down => move$.pipe(takeUntil(up$))));

    const onDrag = e => {
      this.setValueOnParent(e, this.outerBarRef.current.getBoundingClientRect());
    };

    const onClick = e => {
      this.setValueOnParent(e, this.outerBarRef.current.getBoundingClientRect());
    };

    drag$.subscribe(onDrag); 
    down$.subscribe(onClick);
  }

  setValueOnParent(e, boundingClientRect) {
    const { range, updateParent, stateKey, type, fetchBrowseRecommendations } = this.props;
    const [ rangeStart, rangeEnd ] = range;
    const rangeMagnitude = rangeEnd - rangeStart;

    const { 
      x : outerBarXOffset, 
      width : outerBarWidth
    } = boundingClientRect;
    
    const leftPx = e.clientX - outerBarXOffset;
    let leftPct = leftPx / outerBarWidth;
    
    if(leftPct > 1) {
      leftPct = 1;
    } else if (leftPct < 0) {
      leftPct = 0;
    }
    
    // If integer, coerce float to integer
    let value = rangeStart + (rangeMagnitude * leftPct);
    if( type === "integer") {
      value = value.toFixed();
    }
    if( type === "float") {
      value = value.toFixed(1);
    }

    updateParent({ [stateKey] : value });
    fetchBrowseRecommendations();

  }

  render() {
    const { range, value, label, stateKey, updateParent } = this.props; 
    const [rangeStart, rangeEnd] = range;

    const rangeMagnitude = rangeEnd - rangeStart; 
    
    let leftPct = (value / rangeMagnitude) * 100;

    return (
      <localStyle.BarWrapper>
        <div style={{ display : "flex", justifyContent : "space-between", marginBottom : 5 }}>
          <div>{rangeStart}</div>
          <div>{rangeEnd}</div>
        </div>
        
        <localStyle.BarInvisibleHitBox ref={this.outerBarRef}>
          <localStyle.BarVisibleLine />
          <localStyle.BarLevelIndicator
            ref={this.innerBarRef} 
            leftPct={leftPct}
            isNull={value === null}>
            {
              value && <localStyle.BarValue>
                {value}
              </localStyle.BarValue>
            }
          </localStyle.BarLevelIndicator>
          
        </localStyle.BarInvisibleHitBox>
        
        <localStyle.BarLabel isNull={value === null}>
          {label} 
          {
            value !== null && (
              <React.Fragment>
                &nbsp;&nbsp;
                <localStyle.BarClearButton onClick={() => updateParent({ [stateKey] : null })}>
                  Clear
                </localStyle.BarClearButton>
              </React.Fragment>
            )
          }
        </localStyle.BarLabel>
      </localStyle.BarWrapper>
    )
  }
}

const mapStateToProps = _state => ({

});

const mapDispatchToProps = dispatch => bindActionCreators({

}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Component);