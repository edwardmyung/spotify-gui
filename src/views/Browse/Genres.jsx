import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {actions} from "../../redux/actions";
import {Link} from "react-router-dom";

class Component extends React.Component {
  componentDidMount() {
    const {fetchBrowseCategories} = this.props;
    fetchBrowseCategories();
  }
  render() {
    const {genreCategories, genreCategoriesLoading} = this.props;
    
    if(genreCategoriesLoading) {
      return <div>Loading</div>;
    }
    
    return (
      <div style={{ display : "flex", flexWrap : "wrap"}}>
        { genreCategories.map(genreCategory => (
          <Link  key={genreCategory.href} to={`/genre/${genreCategory.id}`}>
            <div>
              <img src={genreCategory.icons[0].url}/>
              <br/>
              {genreCategory.name}            
            </div>
          </Link>
        ))}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  genreCategories : state.app.browse.genreCategories,
  genreCategoriesLoading : state.app.browse.genreCategoriesLoading
});

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchBrowseCategories : actions.browse.fetchBrowseCategories
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Component);