import styled from "styled-components";
const BAR_HEIGHT = 10;

export const GenreSeed = styled.div`
  background: ${p => (p.isSelected ? p.theme.colors.brand.main : "none")};
  display: inline-block;
  margin: 2px;
  padding: 1px 5px;
  border-radius: 3px;
  cursor: pointer;
  user-select: none;

  color: ${p => (p.isSelected ? "rgba(255, 255, 255, 1)" : "rgba(255, 255, 255, 0.7)")};
  border: solid 1px rgba(255, 255, 255, 0.4);

  &:hover {
    border: solid 1px rgba(255, 255, 255, 1);
    color: rgba(255, 255, 255, 1);
  }

  transition: background 0.4s ease-in-out, color 0.2s ease-in-out, border 0.2s ease-in-out;
`;

export const BarWrapper = styled.div`
  width: 33%;
  margin: 10px 0px;
  padding: 0px 20px;
  user-select: none;
`;

export const BarValue = styled.div`
  display: inline-flex;
  align-items: center;
  background: ${p => p.theme.colors.brand.main};
  transform: translateY(-105%);
  padding: 4px 8px;
  border-radius: 5px;
`;

export const BarClearButton = styled.span`
  color: ${p => p.theme.colors.brand.main};
  cursor: pointer;
`;

export const BarInvisibleHitBox = styled.div`
  height: ${BAR_HEIGHT}px;
  position: relative;
  flex-grow: 1;
  display: flex;
  align-items: center;
  cursor: pointer;
`;

export const BarLabel = styled.div`
  text-align: center;
  margin-top: 10px;
  color: ${p => (p.isNull ? "rgba(255,255,255,0.5)" : "white")};
`;

export const BarVisibleLine = styled.div`
  width: 100%;
  height: 3px;
  background: rgba(255, 255, 255, 0.1);
  border-radius: 3px;

  ${BarInvisibleHitBox}:hover & {
    background: rgba(255, 255, 255, 0.5);
  }

  transition: background 0.3s ease-in-out;
`;

export const BarLevelIndicator = styled.div`
  width: ${BAR_HEIGHT}px;
  height: ${BAR_HEIGHT}px;
  border-radius: ${BAR_HEIGHT}px;
  display: flex;
  justify-content: center;
  align-items: center;

  background: ${p => (p.isNull ? "rgba(255,255,255,0.5)" : p.theme.colors.brand.main)};
  position: absolute;
  left: ${p => p.leftPct}%;
  margin-left: -${BAR_HEIGHT / 2}px;
  font-size: 10px;
`;

export const DiscoverStepWrapper = styled.div`
  padding: 10px 20px;
  color: ${p => (p.active ? p.theme.colors.brand.main : "rgba(255,255,255,0.3)")};
  ${p => (p.active ? p.theme.typography.primary.h5.bold : p.theme.typography.primary.p.regular)}

  transition : font-size 0.3s ease-in-out;
`;

export const DiscoverStepWrapperBlurb = styled.div`
  color: white;
  ${p => p.theme.typography.primary.p.regular}
`;
