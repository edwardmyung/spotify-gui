import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {actions} from "../../redux/actions";
import Track from "../../components/Track";
import {Button} from "../../components";
import DiscoverBar from "./DiscoverBar";
import DiscoverKeys from "./DiscoverKeys";
import DiscoverStepsReadOnly from "./DiscoverStepsReadOnly";
import * as localStyle from "./style";

const MARKET_CODE = "GB";
const LIMIT = 20;
const styles = {
  button : {
    margin : "10px 10px 10px 0px"
  }
}

class Component extends React.Component {
  constructor() {
    super();
    this.state = {
      step : 0,

      targetEnergy : null,
      targetTempo: null, 
      targetValence: null, 
      targetSpeechiness: null, 
      targetPopularity: null,
      targetInstrumentalness: null,
      targetDanceability: null,
      targetKey: null,
      
      selectedGenreSeeds: []
    };

    this.updateState = this.updateState.bind(this);
    this.fetchBrowseRecommendations = this.fetchBrowseRecommendations.bind(this);
    this.checkForTokenInterval = null;
  }

  componentDidMount() {
    const { token, viewBrowseClearDiscoverRecommendationIds } = this.props;

    /*
      As /browse/discover is the first view rendered, the token may legitimately be absent... 
      To deal with this, we use an interval to keep attempting the fetch; within that method
      we do a simple check for the presence of the token (and clear the interval)
    */
   
    if( !token ) {
      this.checkForTokenInterval = setInterval( () => this.fetchGenreSeeds(), 1000);
    }

    viewBrowseClearDiscoverRecommendationIds();
  }

  fetchGenreSeeds() {
    const { token, fetchBrowseGenreSeeds } = this.props;
    if(token) {
      clearInterval(this.checkForTokenInterval);
      fetchBrowseGenreSeeds();
    }
  }

  updateState(nextState) {
    this.setState(nextState);
  }

  onClickGenreSeed(genreSeed) {
    // remove case
    const indexOfGenreSeed = this.state.selectedGenreSeeds.indexOf(genreSeed);
    
    if(indexOfGenreSeed > -1) {
      const newSelectedGenreSeeds = this.state.selectedGenreSeeds;
      newSelectedGenreSeeds.splice(indexOfGenreSeed, 1);

      this.setState({
        selectedGenreSeeds: newSelectedGenreSeeds
      }, function() {
        this.fetchBrowseRecommendations();
      });
      return;
    }

    // add case
    if(this.state.selectedGenreSeeds.length === 5) {
      console.log("Toast: Can only select up to 5 genres")
      return;
    }

    this.setState({
      selectedGenreSeeds : [
        ...this.state.selectedGenreSeeds,
        genreSeed
      ]
    }, () => {
      this.fetchBrowseRecommendations()
    });
  }

  fetchBrowseRecommendations() {
    const {fetchBrowseRecommendations} = this.props;
    if(this.state.selectedGenreSeeds.length > 0) {
      let targets = {
        target_energy: this.state.targetEnergy,
        target_tempo : this.state.targetTempo,
        target_valence: this.state.targetValence,
        target_speechiness: this.state.targetSpeechiness, 
        target_popularity: this.state.targetPopularity,
        target_instrumentalness: this.state.targetInstrumentalness,
        target_danceability: this.state.targetDanceability,
      };

      Object.keys(targets).forEach(key => targets[key] === null && delete targets[key]);

      fetchBrowseRecommendations(LIMIT, MARKET_CODE, this.state.selectedGenreSeeds, targets, this.state.targetKey)
    } 
  }

  renderPrevButton() {
    return (
      <Button 
        onClick={()=> this.setState({ step : this.state.step - 1 })}
        style={styles.button}>
        Back
      </Button>
    )
  }

  renderNextButton(isDisabled) {
    return (
      <Button 
        type="brand"
        onClick={()=> this.setState({ step : this.state.step + 1 })}
        disabled={isDisabled}
        style={styles.button}>
        Next
      </Button>
    )
  }

  renderGenreSeeds() {
    const { genreSeeds, genreSeedsLoading } = this.props;
    
    const clearButton = (
      <Button 
        onClick={ () => this.setState({selectedGenreSeeds : []}) } 
        style={styles.button}>
        Clear
      </Button>
    );

    let body = genreSeedsLoading ? <div>Loading</div> : <React.Fragment>
      {
        genreSeeds.map(genreSeed => {
          return (
            <localStyle.GenreSeed
              key={genreSeed} 
              onClick={ () => this.onClickGenreSeed(genreSeed)}
              isSelected={this.state.selectedGenreSeeds.indexOf(genreSeed) > -1}>
              {genreSeed}
            </localStyle.GenreSeed>
          )
        })
      }
    </React.Fragment>

    return (
      <div>
        <h1>1. Select Genres</h1>
        {body}
        <div>
          {this.state.selectedGenreSeeds.length > 0 && clearButton}
          {this.renderNextButton(this.state.selectedGenreSeeds.length === 0)}
        </div>
      </div>
    );
  }

  renderParameterControls() {
    const bars = [
      {type:"integer", range: [1,300], stateKey:"targetTempo", label : "Beats Per Minute (BPM)", value: this.state.targetTempo},
      {type:"float", range: [0,1], stateKey:"targetValence", label : "Happiness", value: this.state.targetValence},
      {type:"float", range: [0,1], stateKey:"targetSpeechiness", label : "Speechiness", value: this.state.targetSpeechiness},
      {type:"integer", range: [0,100], stateKey:"targetPopularity", label : "Popularity", value: this.state.targetPopularity},
      {type:"float", range: [0,1], stateKey:"targetInstrumentalness", label : "Instrumentalness", value: this.state.targetInstrumentalness},
      {type:"float", range: [0,1], stateKey:"targetDanceability", label : "Danceability", value: this.state.targetDanceability},
      {type:"float", range: [0,1], stateKey:"targetEnergy", label : "Energy", value: this.state.targetEnergy},
    ];

    return (
      <div>
        <h1>2. Set Preferences</h1>
        <div style={{display: "flex", flexWrap : "wrap"}}>
          {bars.map( bar => (
            <DiscoverBar 
              key={bar.stateKey}
              type={bar.type}
              range={bar.range}
              updateParent={this.updateState} 
              stateKey={bar.stateKey}
              label={bar.label}
              value={bar.value}
              fetchBrowseRecommendations={this.fetchBrowseRecommendations}/>
          ))}

          <DiscoverKeys 
            updateParent={this.updateState}
            stateKey="targetKey"
            value={this.state.targetKey}
            fetchBrowseRecommendations={this.fetchBrowseRecommendations}/>
        </div>
          
        {this.renderPrevButton()}
        {this.renderNextButton()}
      </div>
    );
  }

  render() {
    const { tracks, loadingRecommendations } = this.props;
    const trackUris = tracks.map(track => track.uri);
  
    let renderedTracks = loadingRecommendations ? 
      <div>LOADING</div>
      : <div>
        <h1>Your new mix</h1>
        <Button type="brand" style={styles.button}>Save playlist</Button>
        {this.renderPrevButton()}
        <Button style={styles.button}>Restart</Button>
        {tracks.map( track => <Track key={track.id} track={track} trackUris={trackUris}/>)}
      </div>

    let body;

    if(this.state.step === 0) {
      body = this.renderGenreSeeds();
    } else if( this.state.step === 1) {
      body = this.renderParameterControls();
    } else if(this.state.step === 2) {
      body = renderedTracks;
    }

    return (
      <div style={{display : "flex"}}>
        <DiscoverStepsReadOnly step={this.state.step}/>
        <div style={{padding : "0px 20px"}}>
          {body}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {  
  const tracks = state.app.viewBrowse.discoverRecommendationTrackIds.map(trackId => state.entities.track[trackId]);

  return {
    token : state.app.token.token,
    genreSeeds : state.app.browse.genreSeeds,
    genreSeedsLoading : state.app.browse.genreSeedsLoading,
    loadingRecommendations : state.app.browse.discoverRecommendationsLoading,
    tracks
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchBrowseRecommendations : actions.browse.fetchBrowseRecommendations,
  fetchBrowseGenreSeeds : actions.browse.fetchBrowseGenreSeeds,
  viewBrowseClearDiscoverRecommendationIds : actions.viewBrowse.viewBrowseClearDiscoverRecommendationIds
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Component);