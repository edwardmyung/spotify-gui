import {default as colors} from "./colors";
import {default as theme} from "./theme";
import {default as typography} from "./typography";

export default {
    colors,
    theme,
    typography
};
