const PRIMARY_FONT_STACK = "'Nunito Sans', sans-serif;";

export default {
  primary: {
    h1: {},
    h2: {
      regular: `
        font-size : 24px;
        font-weight : 400;
        font-family : ${PRIMARY_FONT_STACK};
      `,
      bold: `
        font-size : 24px;
        font-weight : 700;
        font-family : ${PRIMARY_FONT_STACK};
      `
    },
    h3: {
      regular: `
        font-size : 20px;
        font-weight : 400;
        font-family : ${PRIMARY_FONT_STACK};
      `,
      bold: `
        font-size : 20px;
        font-weight : 700;
        font-family : ${PRIMARY_FONT_STACK};
      `
    },
    h4: {
      regular: `
        font-size : 18px;
        font-weight : 400;
        font-family : ${PRIMARY_FONT_STACK};
      `,
      bold: `
        font-size : 18px;
        font-weight : 700;
        font-family : ${PRIMARY_FONT_STACK};
      `
    },
    h5: {
      regular: `
        font-size : 16px;
        font-weight : 400;
        font-family : ${PRIMARY_FONT_STACK};
      `,
      bold: `
        font-size : 16px;
        font-weight : 700;
        font-family : ${PRIMARY_FONT_STACK};
      `
    },
    p: {
      regular: `
        font-size : 14px;
        font-weight : 400;
        font-family : ${PRIMARY_FONT_STACK};
      `,
      bold: `
        font-size : 14px;
        font-weight : 700;
        font-family : ${PRIMARY_FONT_STACK};
      `
    },
    pSmall: {
      regular: `
        font-size : 12px;
        font-weight : 400;
        font-family : ${PRIMARY_FONT_STACK};
      `,
      regularUppercase: `
        font-size : 12px;
        font-weight: 400;
        font-family: ${PRIMARY_FONT_STACK};
        text-transform: uppercase;
        letter-spacing : 3px;
      `,
      bold: `
        font-size : 12px;
        font-weight : 700;
        font-family : ${PRIMARY_FONT_STACK};
      `
    }
  }
};
