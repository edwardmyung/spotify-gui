import React from "react";
import qs from "qs";
import moment from "moment";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {actions} from "./redux/actions";

import {Sidenav, Player, style} from "./components";
import {Browse, Search, Collection, Playlist, Artist, Album, User, Genre} from "./views";
import constants from "./constants";

class App extends React.Component {
  componentDidMount() {
    const {expiry_dt} = this.props;

    const isExpired = moment(expiry_dt) < moment.now();
    if (this.props.token && !isExpired) {
      /*
        If this token is already present, it was in the state saved in localStorage...
        Therefore, no need to do anything related to authentication
      */
      return;
    }

    localStorage.removeItem(constants.localStorage.reduxStore);

    let hashParams = {};
    let e,
      r = /([^&;=]+)=?([^&;]*)/g,
      q = window.location.hash.substring(1);

    while ((e = r.exec(q))) {
      hashParams[e[1]] = decodeURIComponent(e[2]);
    }

    if (!hashParams.access_token) {
      const scopes = [
        "playlist-read-collaborative",
        "playlist-modify-private",
        "playlist-modify-public",
        "playlist-read-private",
        "user-modify-playback-state",
        "user-read-currently-playing",
        "user-read-playback-state",
        "user-read-private",
        "user-read-email",
        "user-library-modify",
        "user-library-read",
        "user-follow-modify",
        "user-follow-read",
        "user-read-recently-played",
        "user-top-read",
        "streaming",
        "app-remote-control"
      ];
      const redirectUrl = "http://localhost:8080/browse/discover";

      window.location.href = `https://accounts.spotify.com/authorize?${qs.stringify({
        client_id: process.env.SPOTIFY_CLIENT_ID,
        scope: scopes.join(" "),
        response_type: "token",
        redirect_uri: redirectUrl
      })}`;
    } else {
      const momentNow = moment(moment.now()).add(1, "hour");
      this.props.setToken({
        token: hashParams.access_token,
        expiry_dt: momentNow.valueOf() // converts to unix timestamp
      });
      this.props.fetchPlaylistsMine(constants.view.PAGE_SIZE_PLAYLISTS_MINE, 0);
      this.props.fetchCurrentUser();
    }
  }
  render() {
    return (
      <Router>
        <style.layout.Wrapper>
          <style.layout.Body>
            <Sidenav />

            <Switch>
              <Route path="/browse" component={Browse} />
              <Route exact path="/search" component={Search} />
              <Route path="/collection" component={Collection} />
              <Route exact path="/artist/:artistId" component={Artist} />
              <Route exact path="/album/:albumId" component={Album} />
              <Route exact path="/user/:userId" component={User} />
              <Route exact path="/playlist/:playlistId" component={Playlist} />
              <Route exact path="/genre/:genreCategoryId" component={Genre} />
            </Switch>
          </style.layout.Body>

          <style.layout.Footer>
            <Player />
          </style.layout.Footer>
        </style.layout.Wrapper>
      </Router>
    );
  }
}

const mapStateToProps = state => ({
  token: state.app.token.token,
  expiry_dt: state.app.token.expiry_dt
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setToken: actions.token.setToken,
      fetchPlaylistsMine: actions.playlist.fetchPlaylistsMine,
      fetchCurrentUser: actions.user.fetchCurrentUser
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
