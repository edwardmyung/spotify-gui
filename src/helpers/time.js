import moment from "moment";

export const formatMsAsReadable = ms => {
  let fullFormattedTime = moment.utc(ms).format("HH:mm:ss");
  let isZeroOrColon = true;
  let startIx = 0;

  const MIN_CHARS = 4;

  for (let char of fullFormattedTime) {
    isZeroOrColon = char === "0" || char === ":";

    if (isZeroOrColon && startIx <= fullFormattedTime.length - MIN_CHARS - 1) {
      startIx += 1;
    } else {
      break;
    }
  }

  return fullFormattedTime.slice(startIx);
};
